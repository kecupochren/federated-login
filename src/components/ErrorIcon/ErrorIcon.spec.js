import React from 'react'
import { shallow } from 'enzyme'
import { ErrorIcon } from './ErrorIcon'

describe('<ErrorIcon />', () => {
  const props = {}

  it('renders correctly', () => {
    const tree = shallow(<ErrorIcon {...props} />)

    expect(tree).toMatchSnapshot()
  })
})
