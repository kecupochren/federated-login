// @flow

import React from 'react'

import styles from './styles.scss'

export const ErrorIcon = () => (
  <div className={styles.wrapper}>
    <div className={styles.mark}>
      <div className={styles.lineLeft} />
      <div className={styles.lineRight} />
    </div>
  </div>
)

ErrorIcon.displayName = 'ErrorIcon'

export default ErrorIcon
