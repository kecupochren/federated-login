// @flow

import React, { Component } from 'react'
import cx from 'classnames'
import get from 'lodash/get'
import { injectIntl } from 'react-intl'
import { Field } from 'redux-form'
import RTInput from 'react-toolbox/lib/input/index'
import type { MessageDescriptor, IntlShape } from 'react-intl'

import styles from './styles.scss'

type Props = {
  name: string,
  intl: IntlShape,
  type?: string,
  onChange?: Function,
  messages: {
    label: MessageDescriptor,
  },
}

export class Input extends Component<Props, *> {
  static defaultProps = {
    type: 'text',
    onChange: () => {},
  }

  props: Props

  handleChange = (value: string, field: Object) => {
    // Call parent listener
    // istanbul ignore else
    if (this.props.onChange) {
      this.props.onChange(value)
    }

    // Update redux-form value
    field.input.onChange(value)
  }

  renderField = (field: Object) => {
    const { messages, intl, type, onChange, ...rest } = this.props
    const { input } = field

    const label = get(messages, 'label') && intl.formatMessage(messages.label)
    /* istanbul ignore next */
    const usePasswordClass = type === 'password' && input.value

    return (
      <RTInput
        theme={styles}
        className={cx({ [styles.password]: usePasswordClass })}
        label={label}
        value={input.value}
        type={type}
        {...input}
        {...rest}
        onChange={e => this.handleChange(e, field)}
      />
    )
  }

  render() {
    const { name } = this.props

    return <Field name={name} component={this.renderField} />
  }
}

export default injectIntl(Input)
