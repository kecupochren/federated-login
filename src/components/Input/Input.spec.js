import React from 'react'
import { shallow } from 'enzyme'

import { Input } from './Input'

describe('<Input />', () => {
  const props = {
    name: 'foobar',
    messages: {
      label: { id: 'label.id' },
    },
    onChange: jest.fn(),
    intl: {
      formatMessage: jest.fn(),
    },
  }

  const fieldProps = {
    input: {
      value: 'foobar',
      onChange: jest.fn(),
    },
  }

  it('renders correctly', () => {
    const tree = shallow(<Input {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders field correctly', () => {
    const field = { input: { value: 'foobar' } }

    const tree = shallow(<Input {...props} />)
      .instance()
      .renderField(field)

    expect(tree).toMatchSnapshot()
  })

  it('renders field label correctly', () => {
    shallow(<Input {...props} />)
      .instance()
      .renderField(fieldProps)

    expect(props.intl.formatMessage).toBeCalledWith(props.messages.label)
  })

  it('handles change', () => {
    const tree = shallow(<Input {...props} />)
    const value = 'foobar'

    tree.instance().handleChange(value, fieldProps)

    expect(props.onChange).toBeCalledWith(value)
  })
})
