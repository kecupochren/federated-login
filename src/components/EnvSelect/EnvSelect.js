// @flow

import React from 'react'
import { connect } from 'react-redux'

import { configActions } from '../../redux'
import { Environments } from '../../constants'

type Props = {
  setEnvironment: (env: string) => mixed,
}

// istanbul ignore next
const EnvSelect = ({ setEnvironment }: Props) => (
  <div style={{ position: 'absolute', right: '10px', bottom: '10px' }}>
    {Object.keys(Environments).map(env => (
      <button key={env} onClick={() => setEnvironment(env)}>
        {env}
      </button>
    ))}
  </div>
)

const withStore = connect(null, {
  setEnvironment: configActions.setEnvironment,
})

export default withStore(EnvSelect)
