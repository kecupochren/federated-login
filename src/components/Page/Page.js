// @flow

import * as React from 'react'
import cx from 'classnames'

import { Logo } from '../../components'
import { LanguageSelector } from './components'

import styles from './styles.scss'

type Props = {
  className?: string,
  compact?: boolean,
  children: any,
}

export const Page = ({ children, className = '', compact, ...rest }: Props) => (
  <div className={styles.wrapper} {...rest}>
    <header>
      <div className={styles.headerContent}>
        <Logo />
        <LanguageSelector />
      </div>
    </header>

    <main>
      <div
        className={cx(styles.content, {
          [className]: className,
          [styles.compact]: compact,
        })}>
        {children}
      </div>
    </main>
  </div>
)

export default Page
