// @flow

import React from 'react'
import { BSpinner } from '@prague-digi/ui-components'

import styles from './styles.scss'

type Props = {
  isVisible: boolean,
}

const Spinner = ({ isVisible }: Props) => (
  <div className={styles.wrapper}>
    <BSpinner isShown={isVisible} />
  </div>
)

export default Spinner
