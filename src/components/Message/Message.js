// @flow

import React from 'react'
import cx from 'classnames'
import { FormattedHTMLMessage, injectIntl } from 'react-intl'
import type { IntlShape } from 'react-intl'

import styles from './styles.scss'

type Props = {
  isVisible: boolean,
  error: boolean,
  success: boolean,
  className: string,
  message: any,
  intl: IntlShape,
}

export const PADDING = 40

export const Message: Object = ({ isVisible, message, error, success, className, intl }: Props) => {
  let paddingMultiplier = 1

  // istanbul ignore else
  if (message) {
    const formattedMsg = intl.formatMessage(message)

    if (formattedMsg && formattedMsg.length > 55) {
      // For every 55 chars ~ 1 line, add one multiple. Ignore the default, hence -1
      paddingMultiplier = Math.ceil(formattedMsg.length / 55) - 1
    }
  }

  return (
    <span
      testid="wrapper"
      className={cx(styles.message, {
        [styles.error]: error,
        [styles.success]: success,
        [styles.active]: isVisible,
        [className]: [className],
      })}
      style={isVisible ? { paddingBottom: `${PADDING * paddingMultiplier}px` } : {}}>
      {message && <FormattedHTMLMessage {...message} values={message.values} />}
    </span>
  )
}

export default injectIntl(Message)
