import React from 'react'
import { shallow } from 'enzyme'

import { Message, PADDING } from './Message'
import { Errors } from '../../constants'

describe('<Message />', () => {
  const props = {
    error: true,
    intl: { formatMessage: jest.fn() },
    message: Errors.LOGIN_FAILURE,
  }

  it('renders error correctly', () => {
    const tree = shallow(<Message {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders success correctly', () => {
    const tree = shallow(<Message {...props} error={false} success />)

    expect(tree).toMatchSnapshot()
  })

  it('multiplies padding for every 55 chars', () => {
    const multiple = 3
    const mockLongMessage = new Array(multiple * 55).join('a')
    const intl = { formatMessage: jest.fn().mockImplementation(() => mockLongMessage) }

    const tree = shallow(<Message {...props} intl={intl} isVisible />)
    const wrapper = tree.find('[testid="wrapper"]')

    expect(wrapper.props().style).toEqual({ paddingBottom: `${PADDING * (multiple - 1)}px` })
  })
})
