// @flow

import React from 'react'

import styles from './styles.scss'

export const Logo = ({ ...rest }: Object) => (
  <div className={styles.wrapper} {...rest}>
    <img alt="MONETA logo" src={require('../../assets/img/moneta-logo.svg')} />
  </div>
)

export default Logo
