// @flow

import * as React from 'react'
import cx from 'classnames'
import { PButton } from '@prague-digi/ui-components'

import styles from './styles.scss'

type Props = {
  anchor?: boolean,
  center?: boolean,
  className?: string,
  disabled?: boolean,
  onClick?: Function,
  resetMargin?: boolean,
  secondary?: boolean,
  submitting?: boolean,
}

export const Button = ({
  anchor,
  center,
  className,
  disabled,
  onClick,
  resetMargin,
  secondary,
  submitting,
  ...rest
}: Props) => {
  const handleClick = (e: any) => {
    // istanbul ignore else
    if (disabled || submitting) {
      e.preventDefault()
    } else if (onClick) {
      onClick(e)
    }
  }

  return (
    <div
      className={cx(styles.wrapper, {
        'mb-2': !resetMargin,
        [styles.disabled]: disabled,
        [styles.submitting]: submitting,
        [styles.center]: center,
        [className || '']: className,
      })}>
      <PButton
        {...rest}
        testid="button"
        className={cx({
          [styles.button]: !anchor,
          [styles.anchor]: anchor,
          [styles.secondary]: secondary,
        })}
        onClick={handleClick}
      />
    </div>
  )
}

export default Button
