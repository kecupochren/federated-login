import React from 'react'
import { shallow } from 'enzyme'
import Button from './Button'

describe('<Button />', () => {
  const props = {
    onClick: jest.fn(),
  }

  it('renders correctly', () => {
    const tree = shallow(<Button {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders disabled correctly', () => {
    const tree = shallow(<Button {...props} disabled submitting />)

    expect(tree).toMatchSnapshot()
  })

  it("doesn't call onClick when disabled", () => {
    const event = { preventDefault: jest.fn() }
    const tree = shallow(<Button {...props} disabled />)

    tree.find('[testid="button"]').simulate('click', event)

    expect(props.onClick).not.toBeCalled()
    expect(event.preventDefault).toBeCalled()
  })

  it("doesn't call onClick when submitting", () => {
    const event = { preventDefault: jest.fn() }
    const tree = shallow(<Button {...props} submitting />)

    tree.find('[testid="button"]').simulate('click', event)

    expect(props.onClick).not.toBeCalled()
    expect(event.preventDefault).toBeCalled()
  })

  it('calls onClick when neither submitting or disabled', () => {
    const event = { preventDefault: jest.fn() }
    const tree = shallow(<Button {...props} />)

    tree.find('[testid="button"]').simulate('click', event)

    expect(props.onClick).toBeCalled()
    expect(event.preventDefault).not.toBeCalled()
  })
})
