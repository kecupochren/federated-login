import React from 'react'
import { shallow } from 'enzyme'

import { Text } from './Text'

describe('<Text />', () => {
  const props = {
    message: { id: 'label.id', defaultMessage: 'ID' },
    values: { foo: 'bar' },
    children: 'Foobar',
  }

  it('renders correctly', () => {
    const tree = shallow(<Text {...props} />)

    expect(tree).toMatchSnapshot()
  })
})
