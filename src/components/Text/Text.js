// @flow

import React from 'react'
import cx from 'classnames'
import { FormattedHTMLMessage } from 'react-intl'
import type { MessageDescriptor } from 'react-intl'

import styles from './styles.scss'

type Props = {
  bold?: boolean,
  center?: boolean,
  children?: any,
  className: string,
  message?: MessageDescriptor | string,
  noMargin?: boolean,
  small?: boolean,
  breakWord?: boolean,
  breakLine?: boolean,
  values?: Object,
}

export const Text = ({
  message,
  children,
  className,
  values,
  bold,
  center,
  noMargin,
  small,
  breakWord,
  breakLine,
  ...rest
}: Props) => (
  <div
    className={cx(styles.wrapper, {
      [className]: className,
      'text--sm': small,
      'text--center': center,
      'text--bold': bold,
      'mb-0': noMargin,
      'text--break': breakWord,
      'text--linebreak': breakLine,
    })}
    {...rest}>
    {message &&
      (typeof message === 'string' ? (
        message
      ) : (
        <FormattedHTMLMessage {...message} values={values} />
      ))}
    {children && children}
  </div>
)

Text.defaultProps = {
  className: '',
}

export default Text
