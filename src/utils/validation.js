// @flow

import * as React from 'react'
import { FormattedMessage } from 'react-intl'

type ValidationMessage = any

export const required = (value: string): ValidationMessage => {
  if (!value) {
    return <FormattedMessage id="validation.required" defaultMessage="Required field" />
  }

  return undefined
}

export function minLength(min: number): ValidationMessage {
  return (value?: string) => {
    if (value && value.length < min) {
      return (
        <FormattedMessage
          id="validation.min_length"
          defaultMessage={`Must be at least ${min} characters`}
          values={{ min }}
        />
      )
    }

    return undefined
  }
}
