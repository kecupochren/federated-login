// @flow

import get from 'lodash/get'

export const isResponseOk = (res: Object): boolean => Boolean(get(res, 'data.status') === 'OK')

export const getResponseData = (res: Object): Object => get(res, 'data.responseObject')

export const mockResponse = (status: string, responseObject: Object): Object => ({
  data: { status, responseObject },
})

export const mockAction = (payload: Object): Object => ({ payload })

export const isInternationalPayment = (paymentData: ?Object) => {
  const code = get(paymentData, 'paymentTypeInformation.serviceLevel.code')
  return code === 'FPOF' || code === 'FPOE'
}
