import { required, minLength } from './validation'

describe('required validator', () => {
  it('returns error when input is empty', () => {
    expect(required()).toMatchSnapshot()
  })

  it('returns undefined when input is not empty', () => {
    expect(required('something')).toBeUndefined()
  })
})

describe('minLength validator', () => {
  it('returns error when value is less than limit', () => {
    expect(minLength(6)('Foo')).toMatchSnapshot()
  })

  it('returns undefined when value is not less than limit', () => {
    expect(minLength(6)('Foobarbaz')).toBeUndefined()
  })
})
