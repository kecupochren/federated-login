import createApi from './api'

describe('createApi()', () => {
  it('creates API matching snapshot', () => {
    const config = { apiUrl: 'https://foo.bar' }
    const api = createApi(config)

    expect(api).toMatchSnapshot()
  })
})
