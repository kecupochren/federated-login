// @flow

import axios from 'axios'

import { Endpoints } from '../constants'
import type { ConfigState } from '../types'

export default function createApi(config: ConfigState) {
  const api = axios.create({
    baseURL: config.apiUrl,
    withCredentials: true,
    headers: { Channel: 23 },
  })

  // prettier-ignore
  // istanbul ignore next
  return {
    login: (data: Object): Promise<Object> => api.post(Endpoints.LOGIN, data),
    sendSmsKey: (): Promise<Object> => api.post(Endpoints.SEND_SMS_KEY),
    verifySmsKey: (data: Object): Promise<Object> => api.post(Endpoints.VERIFY_SMS_KEY, data),
    generateToken: (): Promise<Object> => api.get(Endpoints.GENERATE_TOKEN),
    getPaymentData: (paymentId: string): Promise<Object> => api.get(Endpoints.GET_PAYMENT_DATA(paymentId)),
    sendPaymentSmsKey: (paymentId: string): Promise<Object> => api.post(Endpoints.SEND_PAYMENT_SMS_KEY(paymentId)),
    verifyPaymentSmsKey: ({ paymentId, smsKey }: { paymentId: string, smsKey: string }): Promise<Object> => api.put(Endpoints.VERIFY_PAYMENT_SMS_KEY(paymentId, smsKey)),
    getAccountBalance: (accountId: string, currency: string): Promise<Object> => api.get(`${Endpoints.GET_ACCOUNT_BALANCE(accountId)}?currency=${currency}`),
    grantScopedConsent: () => api.post(Endpoints.GRANT_SCOPED_CONSENT)
  }
}
