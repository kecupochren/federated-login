import createApi from './api'
import mockApi from './mockApi'

describe('MockApi', () => {
  it('has the same endpoints as actual API', () => {
    const api = createApi({ apiUrl: 'foobar' })
    const apiKeys = Object.keys(api)
    const mockApiKeys = Object.keys(mockApi)

    expect(apiKeys).toEqual(mockApiKeys)
  })
})
