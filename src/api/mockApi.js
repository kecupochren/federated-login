// @flow

import loginRes from '../../mockserver/mocks/session/ib/login/POST'
import sendSmsKeyRes from '../../mockserver/mocks/session/ib/sms/send/POST'
import verifySmsKeyRes from '../../mockserver/mocks/session/ib/sms/verify/POST'
import generateTokenRes from '../../mockserver/mocks/secured/psd/login/generate/token/GET'
import getPaymentDataRes from '../../mockserver/mocks/secured/psd/pisp/my/payments/123/GET'
import sendPaymentSmsKeyRes from '../../mockserver/mocks/secured/psd/pisp/my/payments/123/send/otp/POST'
import verifyPaymentSmsKeyRes from '../../mockserver/mocks/secured/psd/pisp/my/payments/123/sign/12341234/PUT'
import getAccountBalanceRes from '../../mockserver/mocks/secured/psd/aisp/my/accounts/021452099/balance/GET'
import grantScopedConsentRes from '../../mockserver/mocks/secured/consent/scopes/POST'

// istanbul ignore next
const mockApi = {
  login: (): Promise<Object> => Promise.resolve(loginRes.body),
  sendSmsKey: (): Promise<Object> => Promise.resolve(sendSmsKeyRes.body),
  verifySmsKey: (): Promise<Object> => Promise.resolve(verifySmsKeyRes.body),
  generateToken: (): Promise<Object> => Promise.resolve(generateTokenRes.body),
  getPaymentData: (): Promise<Object> => Promise.resolve(getPaymentDataRes.body),
  sendPaymentSmsKey: (): Promise<Object> => Promise.resolve(sendPaymentSmsKeyRes.body),
  verifyPaymentSmsKey: (): Promise<Object> => Promise.resolve(verifyPaymentSmsKeyRes.body),
  getAccountBalance: (): Promise<Object> => Promise.resolve(getAccountBalanceRes.body),
  grantScopedConsent: (): Promise<Object> => Promise.resolve(grantScopedConsentRes.body),
}

export default mockApi
