// @flow
export default {
  'app.greeting.noName':
    'Kein Name gefunden, versuchen Sie bitte den Stammordner (/) besuchen und das Formular senden.',
}
