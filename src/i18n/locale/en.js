// @flow

// prettier-ignore
export default {
  // Common
  'label.id': 'ID',
  'label.password': 'Password',
  'label.sms_key': 'Mobile key',

  'button.confirm_access.TOKEN': 'Yes, I want to grant access',
  'button.confirm_access.IDENTITY_CONFIRMATION': 'Yes, I want to confirm my identity',
  'button.reject_access': 'No, send me back to vendor', 
  'button.grant_access': 'Grant access',
  'button.continue': 'Continue',
  'button.send_sms_key': 'Send mobile key',
  'button.new_sms_key': 'New mobile key',
  'button.return_to_vendor': 'Return to vendor',
  'button.develop_mode_redirect': 'Redirect to test URL',
  'button.verify_sms_key': 'Verify mobile key',

  'validation.required': 'Required field',
  'validation.min_length': '{min} characters minimum',

  // Messages
  'error.login_failure': 'Incorrect login credentials. The number of attempts is limited to 3. To unblock your account, please contact our Customer Service Center at any MONETA Money Bank branch.',
  'error.send_sms_key_disabled': 'Mobile key login has been blocked. To unblock your account, contact our Customer support or visit a bank branch that manages your account',  
  'error.verify_sms_key_incorrect': 'Incorrect mobile key',
  'error.verify_sms_key_expired': 'Expired mobile key',
  'error.generate_token_failure': 'Unknown failure',
  'error.unknown_failure': 'Unknown failure',
  'error.get_payment_data_failure': 'Sorry, your MONETA API payment request has been expired.',
  'error.get_account_balance_failure': 'Unknown failure',
  'error.transaction_missing': 'Sorry, your MONETA API payment request has been expired.',
  'error.general_technical_error': 'The process could not be completed due to technical difficulties.',

  'success.resend_sms': 'A new mobile key has been sent.',
  
  // Pages
  'page.success.TOKEN.title': 'Access has been successfully granted. <br /><br /> We will now return you back to the vendor',
  'page.success.IDENTITY_CONFIRMATION.title': 'Your identity has been successfully confirmed. <br /><br /> We will now return you back to the vendor.',
  'page.success.IDENTITY_CONFIRMATION.title.KYC': 'Your personal details have been sent successfully. <br /><br /> We will now return you back to the vendor.',
  'page.success.PAYMENT.title': 'Payment has been successfully authorized. <br /><br /> We will now return you back to the vendor.',

  'page.login.TOKEN.title': 'The application "<strong>{thirdParty}</strong>" is requesting access to your account',
  'page.login.IDENTITY_CONFIRMATION.title': 'The company "<strong>{thirdParty}</strong>" is requesting your identity confirmation.',
  'page.login.PAYMENT.title': 'Login first to authorize the payment request',

  'page.login.permissions.title': 'By logging in you give the application "<strong>{application}</strong>" access to:',
  'page.login.permissions.title.KYC': 'By continuing you agree with providing your personal data to "<strong>{application}</strong>" in the following extent:',
  'page.login.permissions.AISP': '<strong>Paymount account information</strong> - Providing information about the payment account selected by you, in the extent as it available in your internet banking, namely, information about your transaction history, standing orders and other information connected with your payment account.',
  'page.login.permissions.PISP': '<strong>Indirect access for the payment initiation service provider to your payment account</strong> – You may provide indirect access for the payment initiation service provider up to the BankAPI limit. The default limit setting is the same as the limit for active transactions in your Internet Banka. The payment initiation service provider will receive all information concerning the relevant transaction.',
  'page.login.permissions.KYC': 'Name and surname, birth number – where a birth number is not assigned, date of birth, place of birth, gender, permanent address – or a temporary address, citizenship, type and number of your primary ID document in order to verify your identity online in accordance with Act no. 253/2008 Coll., on Selected measures against the legalisation of proceeds of crime and financing of terrorism.',
  'page.login.permissions.note': 'MONETA Money Bank, a.s. is not liable for any use of this shared data by "<strong>{application}</strong>".',

  'page.payment.title': 'Payment order confirmation',
  'page.payment.amount': 'Amount:',
  'page.payment.variable_symbol': 'Variable symbol:',
  'page.payment.constant_symbol': 'Constant symbol:',
  'page.payment.specific_symbol': 'Specific symbol:',
  'page.payment.due_date': 'Due date:',
  'page.payment.receiver_message': 'Message for recipient:',
  'page.payment.from_account': 'From account:',
  'page.payment.account_balance': 'Account balance:',
  'page.payment.receiver_name': 'Beneficiary\'s name:',
  'page.payment.receiver_bank_number': 'Beneficiary\'s account number:',
  'page.payment.receiver_bank_code': 'Beneficiary bank code:',
  'page.payment.sms_notice': 'Please note the text of the SMS with the mobile key. It contains a description of the operation you authorize. If the data in the SMS corresponds to your input, confirm the operation with the mobile key.',
  // International payment
  'page.payment.own_account': 'Own account:',
  'page.payment.expenses': 'Expenses:',
  'page.payment.iban': 'Account number (IBAN):',
  'page.payment.receiver': 'Name and address of the beneficiary:',
  'page.payment.payer': 'Payer\'s reference:',
  'page.payment.bic': 'Bank Identity Code (BIC):',
  'page.payment.bank': 'Bank name and address:',
  'page.payment.country': 'Country:',
  'page.payment.info': 'Transaction confirmation:'
}
