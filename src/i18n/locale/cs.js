// @flow

// prettier-ignore
export default {
  // Common
  'label.id': 'ID',
  'label.password': 'Heslo',
  'label.sms_key': 'Zadejte mobilní klíč',

  'button.confirm_access.TOKEN': 'Ano, chci udělit přístup',
  'button.confirm_access.IDENTITY_CONFIRMATION': 'Ano, chci ověřit svou identitu',
  'button.reject_access': 'Ne, vrátit na stránky obchodníka', 
  'button.grant_access': 'Udělit přístup',
  'button.continue': 'Pokračovat',
  'button.send_sms_key': 'Odeslat mobilní klíč',
  'button.new_sms_key': 'Nový mobilní klíč',
  'button.return_to_vendor': 'Zpět na stránky obchodníka',
  'button.develop_mode_redirect': 'Redirect to test URL',
  'button.verify_sms_key': 'Ověřit mobilní klíč',

  'validation.required': 'Pole musí být vyplněno',
  'validation.min_length': 'Hodnota musí být minimálně {min} znaků',

  // Messages
  'error.login_failure': 'Nesprávné přihlašovací údaje. Počet pokusů je omezen na 3. Pro případné odblokování kontaktujte náš Zákaznický servis nebo navštivte kteroukoliv pobočku MONETA Money Bank.',
  'error.send_sms_key_disabled': 'Přihlášení mobilním klíčem Vám bylo zablokováno. Pro odblokování kontaktujte náš Zákaznický servis nebo navštivte pobočku banky, která vede Váš účet.',  
  'error.verify_sms_key_incorrect': 'Nesprávný mobilní klíč',
  'error.verify_sms_key_expired': 'Platnost mobilního klíče vypršela',
  'error.generate_token_failure': 'Při udělování přístupu došlo k neznámé chybě.',
  'error.unknown_failure': 'Neznámá chyba',
  'error.get_payment_data_failure': 'Vážený kliente, Váš požadavek na platbu MONETA API již není platný.',
  'error.get_account_balance_failure': 'Při získávání disponibilního zůstatku došlo k chybě',
  'error.transaction_missing': 'Vážený kliente, Váš požadavek na platbu MONETA API již není platný.',
  'error.general_technical_error': 'Litujeme, proces se nepodařilo dokončit kvůli technickým obtížím.',

  'success.resend_sms': 'Nový mobilní klíč byl úspešne odeslán.',
  
  // Pages
  'page.success.TOKEN.title': 'Udělení přístupu proběhlo úspěšně. <br /><br /> Nyní Vás vrátíme na stránku obchodníka.',
  'page.success.IDENTITY_CONFIRMATION.title': 'Ověření identity proběhlo úspěšně. <br /><br /> Nyní Vás vrátíme na stránku obchodníka.',
  'page.success.IDENTITY_CONFIRMATION.title.KYC': 'Předání Vašich osobních údajů proběhlo úspěšně. <br /><br /> Nyní Vás vrátíme na stránku obchodníka.',
  'page.success.PAYMENT.title': 'Potvrzení platby proběhlo úspěšně. <br /><br /> Nyní Vás vrátíme na stránku obchodníka.',

  'page.login.TOKEN.title': 'Aplikace "<strong>{thirdParty}</strong>" žádá o přístup k vašemu účtu.',
  'page.login.IDENTITY_CONFIRMATION.title': 'Společnost "<strong>{thirdParty}</strong>" žádá o ověření vaší identity.',
  'page.login.PAYMENT.title': 'Pro potvrzení platebního příkazu je nutné se nejprve přihlásit',
  
  'page.login.permissions.title': 'Přihlášením dáváte aplikaci "<strong>{application}</strong>" oprávnění k přístupu&nbsp;k:',
  'page.login.permissions.title.KYC': 'Pokračováním souhlasíte s předáním Vašich osobních údajů společnosti "<strong>{application}</strong>" v rozsahu:',
  'page.login.permissions.AISP': '<strong>Informace o účtu</strong> - poskytnutí informací o vámi vybraném platebním účtu, a to ve stejném rozsahu jako jsou přístupné ve vašem internetovém bankovnictví – zejména tedy k informacím o transakční historii, trvalým příkazům a jiným informacím spojeným s Vaším platebním účtem.',
  'page.login.permissions.PISP': '<strong>Nepřímé dání platebního příkazu z vašeho platebního účtu</strong> – nepřímo můžete dát platební příkaz až do výše limitu pro BankAPI. Výchozí nastavení limitu je totožné s limitem pro aktivní operace ve Vaší Internet Bance. Poskytovateli služby nepřímého dání platebního příkazu tímto zpřístupníte všechny informace týkající se dané transakce.',
  'page.login.permissions.KYC': 'Jméno a příjmení, rodné číslo - případně nebylo-li přiděleno datum narození, místo narození, pohlaví, adresa trvalého pobytu – případně jiná adresa pobytu, státní občanství, typ a číslo primárního dokladu za účelem online ověření Vaší identity v souladu se zákonem č. 253/2008 Sb., o některých opatřeních proti legalizaci výnosů z trestné činnosti a financování terorismu.',
  'page.login.permissions.note': 'MONETA Money Bank, a.s. nenese žádnou odpovědnost za užívání takto sdílených údajů společností "<strong>{application}</strong>".',
  
  'page.payment.title': 'Potvrzení platebního příkazu',
  'page.payment.amount': 'Částka:',
  'page.payment.variable_symbol': 'Variabilní symbol:',
  'page.payment.constant_symbol': 'Konstantní symbol:',
  'page.payment.specific_symbol': 'Specifický symbol:',
  'page.payment.due_date': 'Datum splatnosti:',
  'page.payment.receiver_message': 'Zpráva pro příjemce:',
  'page.payment.from_account': 'Z účtu:',
  'page.payment.account_balance': 'Disponibilní zůstatek:',
  'page.payment.receiver_name': 'Název příjemce:',
  'page.payment.receiver_bank_number': 'Číslo účtu příjemce:',
  'page.payment.receiver_bank_code': 'Kód banky příjemce:',
  'page.payment.sms_notice': 'Věnujte prosím pozornost celému znění SMS zprávy s mobilním klíčem. Obsahuje zároveň popis operace, kterou autorizujete. Pokud údaje uvedené v SMS odpovídají Vašemu zadaní, potvrďte operaci mobilním klíčem.',
  // International payment
  'page.payment.own_account': 'Vlastní účet:',
  'page.payment.expenses': 'Výlohy:',
  'page.payment.iban': 'Číslo účtu (IBAN):',
  'page.payment.receiver': 'Jméno a adresa příjemce:',
  'page.payment.payer': 'Reference plátce:',
  'page.payment.bic': 'Identifikační kód banky (BIC):',
  'page.payment.bank': 'Jméno a adresa banky:',
  'page.payment.country': 'Země:',
  'page.payment.info': 'Zaslání konfirmace:',
}
