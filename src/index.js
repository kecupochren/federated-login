// @flow

import React from 'react'
import store from 'store'
import type { ComponentType } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import errorHandler from '@prague-digi/error-handler'

import { App } from './containers'
import storeInstance from './redux/store'

// Global error handler (listening to the error event of window), passing error.message to callback
errorHandler(alert)

export const render = (Component: ComponentType<any>) => {
  ReactDOM.render(
    <Provider store={storeInstance}>
      <Component />
    </Provider>,
    /* $FlowFixMe */
    document.getElementById('root')
  )
}

render(App)

// istanbul ignore next
if (module.hot) {
  const env = store.get('env') || 'DEV'

  // React HMR
  module.hot.accept('./containers/App/App', () => {
    render(App)
  })

  // Reducer HMR
  module.hot.accept('./redux/store', () => {
    const nextReducer = require('./redux/store').reducer
    store.replaceReducer(nextReducer)
  })

  // Saga HMR, note that when enabled, changes in ./redux will also trigger the reload
  module.hot.accept('./sagas/sagas', () => {
    const nextSagaManager = require('./sagas/sagas').default()
    /* $FlowFixMe */
    nextSagaManager.cancel()
    /* $FlowFixMe */
    nextSagaManager.start(env)
  })
}
