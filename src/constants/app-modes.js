// @flow

/**
 * The application handles multiple grant scenarios
 *
 * We use the constants from here to decide what to render and what to do after successful login
 */

import { PermissionScopes as scopes } from './'

export const modes = {
  // Grant federated token
  TOKEN: 'TOKEN',
  // Authorize payment
  PAYMENT: 'PAYMENT',
  // Confirm identity
  IDENTITY_CONFIRMATION: 'IDENTITY_CONFIRMATION',
}

export const getMode = (scope: string) => {
  switch (scope) {
    case scopes.ALL:
    case scopes.AISP:
    case scopes.PISP:
      return modes.TOKEN
    case scopes.KYC:
      return modes.IDENTITY_CONFIRMATION
    default:
      return null
  }
}

export default modes
