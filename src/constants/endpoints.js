// @flow

// prettier-ignore
// istanbul ignore next
export default {
  LOGIN: '/session/ib/login',
  SEND_SMS_KEY: '/session/ib/sms/send',
  VERIFY_SMS_KEY: '/session/ib/sms/verify',
  GENERATE_TOKEN: '/secured/psd/login/generate/token',
  GET_PAYMENT_DATA: (paymentId: string) => `/secured/psd/pisp/my/payments/${paymentId}/`, // ENDING SLASH REQUIRED!
  SEND_PAYMENT_SMS_KEY: (paymentId: string) => `/secured/psd/pisp/my/payments/${paymentId}/send/otp`,
  VERIFY_PAYMENT_SMS_KEY: (paymentId: string, key: string) => `/secured/psd/pisp/my/payments/${paymentId}/sign/${key}/`,
  GET_ACCOUNT_BALANCE: (accountId: string) => `/secured/psd/aisp/my/accounts/${accountId}/balance`,
  GRANT_SCOPED_CONSENT: '/secured/consent/scopes'
}
