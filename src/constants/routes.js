// @flow

// React Router path definition
// Naming convention = constant-case page container names, e.g. containers/Home uses HOME
export default {
  LOGIN: '/login',
  PAYMENT: '/payment',
  SUCCESS: '/success',
  ERROR: '/error',
}
