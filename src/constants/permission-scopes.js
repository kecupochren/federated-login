// @flow

/**
 * Enum of possible permission scopes.
 *
 * The scope is defined as query paramater, supplied by "CA Technologies" layer, and parsed on the first page load.
 */

export default {
  ALL: 'ALL',
  AISP: 'AISP',
  PISP: 'PISP',
  KYC: 'KYC',
}
