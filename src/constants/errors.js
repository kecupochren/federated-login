// @flow

export default {
  // key: intl message
  LOGIN_FAILURE: {
    id: 'error.login_failure',
    defaultMessage: 'Invalid login credentials',
  },
  GENERATE_TOKEN_FAILURE: {
    id: 'error.generate_token_failure',
    defaultMessage: 'Failed to generate token',
  },
  SEND_SMS_KEY_DISABLED: {
    id: 'error.send_sms_key_disabled',
    defaultMessage: 'Logging in with SMS key has been disabled.',
  },
  VERIFY_SMS_KEY_EXPIRED: {
    id: 'error.verify_sms_key_expired',
    defaultMessage: 'SMS key expired',
  },
  VERIFY_SMS_KEY_INCORRECT: {
    id: 'error.verify_sms_key_incorrect',
    defaultMessage: 'Incorrect SMS key',
  },
  UNKNOWN_FAILURE: {
    id: 'error.unknown_failure',
    defaultMessage: 'Unknown failure',
  },
  GET_PAYMENT_DATA_FAILURE: {
    id: 'error.get_payment_data_failure',
    defaultMessage: 'Failed to get payment data',
  },
  GET_ACCOUNT_BALANCE_FAILURE: {
    id: 'error.get_account_balance_failure',
    defaultMessage: 'Failed to get account balance',
  },
  TRANSACTION_MISSING: {
    id: 'error.transaction_missing',
    defaultMessage: 'The contract has expired',
  },
  GENERAL_TECHNICAL_ERROR: {
    id: 'error.general_technical_error',
    defaultMessage: 'Technical error occured',
  },
}
