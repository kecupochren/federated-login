import { delay } from 'redux-saga'
import { select, call } from 'redux-saga/effects'
import { testSaga, expectSaga } from 'redux-saga-test-plan'
import { change } from 'redux-form'

import MockApi from '../../api/mockApi'
import { Errors } from '../../constants'
import { loginSaga, sendSmsKeySaga, verifySmsKeySaga, resendSmsKeySaga } from './auth'
import { selectors, authActions } from '../../redux'
import { mockResponse, mockAction } from '../../utils'

import { handleVerificationSuccessSaga } from '../app'

/**
 * Login
 */
describe('loginSaga', () => {
  const payload = { id: '123', password: '456' }
  const mocks = response => [
    [select(selectors.getLoginCredentials), payload],
    [call(MockApi.login, payload), response],
  ]

  it('calls login endpoint with correct payload', () => {
    testSaga(loginSaga, MockApi)
      .next()
      .select(selectors.getLoginCredentials)
      .next(payload)
      .call(MockApi.login, payload)
  })

  it('handles thrown errors', () => {
    testSaga(loginSaga, MockApi)
      .next()
      .next()
      .throw()
      .put(authActions.loginFailure())
  })

  it('handles OK response', () => {
    const response = mockResponse('OK')

    return expectSaga(loginSaga, MockApi)
      .provide(mocks(response))
      .put(authActions.loginSuccess())
      .put(authActions.sendSmsKey())
      .run()
  })

  it('handles ERROR response', () => {
    const response = mockResponse('ERROR')

    return expectSaga(loginSaga, MockApi)
      .provide(mocks(response))
      .put(authActions.loginFailure())
      .run()
  })
})

/**
 * Send SMS key
 */
describe('sendSmsKeySaga', () => {
  const mocks = response => [[call(MockApi.sendSmsKey), response]]

  it('clears the verify input and calls correct endpoint', () => {
    testSaga(sendSmsKeySaga, MockApi)
      .next()
      .put(change('verifySms', 'smsKey', ''))
      .next()
      .call(MockApi.sendSmsKey)
  })

  it('handles thrown errors', () => {
    testSaga(sendSmsKeySaga, MockApi)
      .next()
      .next()
      .throw()
      .put(authActions.sendSmsKeyFailure())
  })

  it('handles SMS_SENT response', () => {
    const response = mockResponse('OK', { smsSendCertValue: 'SMS_SENT' })

    return expectSaga(sendSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(authActions.sendSmsKeySuccess())
      .run()
  })

  it('handles SMS_SENT response when isResending', () => {
    const action = mockAction({ isResending: true })
    const response = mockResponse('OK', { smsSendCertValue: 'SMS_SENT' })

    return expectSaga(sendSmsKeySaga, MockApi, action)
      .provide(mocks(response))
      .put(authActions.sendSmsKeySuccess())
      .call(resendSmsKeySaga)
      .run()
  })

  it('handles SMS_NOT_ENABLED response', () => {
    const response = mockResponse('OK', { smsSendCertValue: 'SMS_NOT_ENABLED' })

    return expectSaga(sendSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(authActions.sendSmsKeyFailure(Errors.SEND_SMS_KEY_DISABLED))
      .run()
  })

  it('handles ERROR status response', () => {
    const response = mockResponse('ERROR')

    return expectSaga(sendSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(authActions.sendSmsKeyFailure(Errors.UNKNOWN_FAILURE))
      .run()
  })
})

/**
 * Resend sms key
 */
describe('resendSmsKeySaga', () => {
  it('show/hides resend message', () => {
    testSaga(resendSmsKeySaga, MockApi)
      .next()
      .put(authActions.setSmsResendMessageVisible(true))
      .next()
      .call(delay, 5000)
      .next()
      .put(authActions.setSmsResendMessageVisible(false))
  })
})

/**
 * Verify SMS Key
 */
describe('verifySmsKeySaga', () => {
  const payload = { mobileKey: '123456' }
  const mocks = response => [
    [select(selectors.getSmsKeyData), payload],
    [call(MockApi.verifySmsKey, payload), response],
  ]

  it('handles ERROR response', () => {
    const response = mockResponse('ERROR')

    return expectSaga(verifySmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(authActions.verifySmsKeyFailure(Errors.UNKNOWN_FAILURE))
      .run()
  })

  it('handles OK status', () => {
    const usePaymentFlow = false
    const response = mockResponse('OK', { verifiedValue: 'OK' })

    return expectSaga(verifySmsKeySaga, MockApi)
      .provide(mocks(response, usePaymentFlow))
      .put(authActions.verifySmsKeySuccess())
      .call(handleVerificationSuccessSaga)
      .run()
  })

  it('handles WRONG_MK status', () => {
    const response = mockResponse('OK', { verifiedValue: 'WRONG_MK' })

    return expectSaga(verifySmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(authActions.verifySmsKeyFailure(Errors.VERIFY_SMS_KEY_INCORRECT))
      .run()
  })

  it('handles WRONG_MK_LOCKED status', () => {
    const response = mockResponse('OK', { verifiedValue: 'WRONG_MK_LOCKED' })

    return expectSaga(verifySmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(authActions.verifySmsKeyFailure(Errors.SEND_SMS_KEY_DISABLED))
      .run()
  })

  it('handles SMS_TIMED_OUT status', () => {
    const response = mockResponse('OK', { verifiedValue: 'SMS_TIMED_OUT' })

    return expectSaga(verifySmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(authActions.verifySmsKeyFailure(Errors.VERIFY_SMS_KEY_EXPIRED))
      .run()
  })

  it('handles unknown status', () => {
    const response = mockResponse('OK', { verifiedValue: 'FOOBAR' })

    return expectSaga(verifySmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(authActions.verifySmsKeyFailure(Errors.UNKNOWN_FAILURE))
      .run()
  })
})
