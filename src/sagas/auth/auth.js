// @flow

import { delay } from 'redux-saga'
import { call, put, select } from 'redux-saga/effects'
import get from 'lodash/get'
import { change } from 'redux-form'

import { Errors } from '../../constants'
import { authActions, selectors } from '../../redux'
import { isResponseOk } from '../../utils'
import type { Saga, Action, Api } from '../../types/'

import { handleVerificationSuccessSaga } from '../app'

export function* loginSaga(api: Api): Saga {
  const payload = yield select(selectors.getLoginCredentials)

  try {
    const res = yield call(api.login, payload)

    if (isResponseOk(res)) {
      yield put(authActions.loginSuccess())
      yield put(authActions.sendSmsKey())
    } else {
      yield put(authActions.loginFailure())
    }
  } catch (e) {
    console.error(e) // eslint-disable-line
    yield put(authActions.loginFailure())
  }
}

export function* sendSmsKeySaga(api: Api, action: Action): Saga {
  yield put(change('verifySms', 'smsKey', ''))

  try {
    const res = yield call(api.sendSmsKey)
    const status = get(res, 'data.responseObject.smsSendCertValue')
    const isSent = status === 'SMS_SENT'

    if (isResponseOk(res) && isSent) {
      yield put(authActions.sendSmsKeySuccess())

      if (get(action, 'payload.isResending')) {
        yield call(resendSmsKeySaga)
      }
    } else {
      const error =
        status === 'SMS_NOT_ENABLED' ? Errors.SEND_SMS_KEY_DISABLED : Errors.UNKNOWN_FAILURE

      yield put(authActions.sendSmsKeyFailure(error))
    }
  } catch (e) {
    console.error(e) // eslint-disable-line
    yield put(authActions.sendSmsKeyFailure())
  }
}

export function* resendSmsKeySaga(): Saga {
  yield put(authActions.setSmsResendMessageVisible(true))
  yield call(delay, 5000)
  yield put(authActions.setSmsResendMessageVisible(false))
}

export function* verifySmsKeySaga(api: Api): Saga {
  const payload = yield select(selectors.getSmsKeyData)

  try {
    const res = yield call(api.verifySmsKey, payload)
    const verifiedValue = get(res, 'data.responseObject.verifiedValue')
    const isVerified = verifiedValue === 'OK'

    if (isVerified && isResponseOk(res)) {
      yield put(authActions.verifySmsKeySuccess())
      yield call(handleVerificationSuccessSaga)
    } else if (isResponseOk(res)) {
      let error

      switch (verifiedValue) {
        case 'SMS_TIMED_OUT':
          error = Errors.VERIFY_SMS_KEY_EXPIRED
          break

        case 'WRONG_MK':
          error = Errors.VERIFY_SMS_KEY_INCORRECT
          break

        case 'WRONG_MK_LOCKED':
          error = Errors.SEND_SMS_KEY_DISABLED
          break

        default:
          error = Errors.UNKNOWN_FAILURE
      }

      yield put(authActions.verifySmsKeyFailure(error))
    } else {
      yield put(authActions.verifySmsKeyFailure(Errors.UNKNOWN_FAILURE))
    }
  } catch (e) {
    console.error(e) // eslint-disable-line
    yield put(authActions.verifySmsKeyFailure(Errors.UNKNOWN_FAILURE))
  }
}
