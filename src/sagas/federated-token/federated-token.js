// @flow

import { push } from 'react-router-redux'
import { call, put } from 'redux-saga/effects'

import { Routes } from '../../constants'
import { tokenActions as actions } from '../../redux'
import { isResponseOk, getResponseData } from '../../utils'
import type { Saga, Api } from '../../types'

export function* generateTokenSaga(api: Api): Saga {
  try {
    const res = yield call(api.generateToken)

    if (isResponseOk(res)) {
      const { loginToken } = getResponseData(res)

      yield put(actions.generateTokenSuccess(loginToken))
      yield put(push(Routes.SUCCESS))
    } else {
      throw
    }
  } catch (e) {
    console.error(e) // eslint-disable-line
    yield put(actions.generateTokenFailure())
    yield put(push(Routes.ERROR))
  }
}
