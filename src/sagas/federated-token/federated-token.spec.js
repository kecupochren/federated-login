import { expectSaga, testSaga } from 'redux-saga-test-plan'
import { push } from 'react-router-redux'
import { call } from 'redux-saga/effects'

import MockApi from '../../api/mockApi'
import { Routes } from '../../constants'
import { tokenActions } from '../../redux'
import { mockResponse } from '../../utils'

import { generateTokenSaga } from './federated-token'

/**
 * Generate token
 */
describe('generateTokenSaga', () => {
  const mocks = response => [[call(MockApi.generateToken), response]]

  it('calls verify endpoint with correct payload', () => {
    testSaga(generateTokenSaga, MockApi)
      .next()
      .call(MockApi.generateToken)
  })

  it('handles thrown errors', () => {
    testSaga(generateTokenSaga, MockApi)
      .next()
      .throw()
      .put(tokenActions.generateTokenFailure())
      .next()
      .put(push(Routes.ERROR))
  })

  it('handles OK response', () => {
    const loginToken = 'foobar'
    const response = mockResponse('OK', { loginToken })

    return expectSaga(generateTokenSaga, MockApi)
      .provide(mocks(response))
      .put(tokenActions.generateTokenSuccess(loginToken))
      .put(push(Routes.SUCCESS))
      .run()
  })

  it('handles ERROR response', () => {
    const response = mockResponse('ERROR')

    return expectSaga(generateTokenSaga, MockApi)
      .provide(mocks(response))
      .put(tokenActions.generateTokenFailure())
      .put(push(Routes.ERROR))
      .run()
  })
})
