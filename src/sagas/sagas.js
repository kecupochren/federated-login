// @flow

import { all, select, takeLatest } from 'redux-saga/effects'

import createApi from '../api/api'
import {
  appActions,
  authActions,
  configActions,
  tokenActions,
  paymentActions,
  selectors,
} from '../redux'
import type { Saga, Api } from '../types'

import { initialize, returnToOriginSaga } from './app'
import { loginSaga, sendSmsKeySaga, verifySmsKeySaga } from './auth'
import { generateTokenSaga } from './federated-token'
import { getPaymentDataSaga, sendPaymentSmsKeySaga, verifyPaymentSmsKeySaga } from './payment'

// business logic saga entry point
export function* createApiAndSagas(): Saga {
  const config = yield select(selectors.getConfig)
  const api: Api = createApi(config)

  const watch = (action, saga) => takeLatest(action().type, saga, api)

  yield all([
    takeLatest(appActions.initialize, initialize, api),
    takeLatest(appActions.returnToOrigin, returnToOriginSaga, api),

    takeLatest(authActions.login, loginSaga, api),
    takeLatest(authActions.sendSmsKey, sendSmsKeySaga, api),
    takeLatest(authActions.verifySmsKey, verifySmsKeySaga, api),

    takeLatest(tokenActions.generateToken, generateTokenSaga, api),

    takeLatest(paymentActions.getPaymentData, getPaymentDataSaga, api),
    takeLatest(paymentActions.sendPaymentSmsKey, sendPaymentSmsKeySaga, api),
    takeLatest(paymentActions.verifyPaymentSmsKey, verifyPaymentSmsKeySaga, api),
  ])
}

export default function* rootSaga(): Saga {
  yield takeLatest(configActions.setEnvironment().type, createApiAndSagas)
}
