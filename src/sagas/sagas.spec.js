import createSagaManager from './sagas'

describe('createSagaManager()', () => {
  it('returns saga manager', () => {
    const store = {}
    const sagaManager = createSagaManager(store)

    expect(sagaManager).toBeDefined()
  })
})
