// @flow

import { delay } from 'redux-saga'
import { call, put, select } from 'redux-saga/effects'
import { push } from 'react-router-redux'
import get from 'lodash/get'

import { Routes, Errors } from '../../constants'
import { paymentActions as actions, selectors } from '../../redux'
import { isResponseOk, getResponseData } from '../../utils'
import type { Saga, Api, Action } from '../../types'

export function* getPaymentDataSaga(api: Api): Saga {
  const payload = yield select(selectors.getPaymentId)

  try {
    const res = yield call(api.getPaymentData, payload)
    const data = getResponseData(res)

    const isValid = get(data, 'instructionStatus') === 'FOR_SIGNING'

    // pass transaction missing error message
    if (isResponseOk(res) && isValid) {
      yield put(actions.getPaymentDataSuccess(data))
      yield call(getAccountBalanceSaga, api)
    } else {
      const errorMessage =
        data && data.code === 'TRANSACTION_MISSING'
          ? Errors.TRANSACTION_MISSING
          : Errors.GET_PAYMENT_DATA_FAILURE
      yield put(actions.getPaymentDataFailure(errorMessage))
      yield put(push(Routes.ERROR))
    }
  } catch (e) {
    const message = get(e, 'response.data.responseObject.message')

    // User unathorized, redirect to login
    if (message === 'POWER_AUTH_SIGNATURE_INVALID') {
      yield put(push(Routes.LOGIN))
    } else {
      console.error(e) // eslint-disable-line
      yield put(actions.getPaymentDataFailure())
      yield put(push(Routes.ERROR))
    }
  }
}

export function* getAccountBalanceSaga(api: Api): Saga {
  const payload = yield select(selectors.getAccountId)
  const currency = yield select(selectors.getAccountCurrency)

  try {
    const res = yield call(api.getAccountBalance, payload, currency)

    if (isResponseOk(res)) {
      const data = getResponseData(res)

      yield put(actions.getAccountBalanceSuccess(data))
    } else {
      yield put(actions.getAccountBalanceFailure())
      yield put(push(Routes.ERROR))
    }
  } catch (e) {
    console.error(e) // eslint-disable-line
    yield put(actions.getAccountBalanceFailure())
    yield put(push(Routes.ERROR))
  }
}

export function* sendPaymentSmsKeySaga(api: Api, action: Action): Saga {
  const payload = yield select(selectors.getPaymentId)

  try {
    const res = yield call(api.sendPaymentSmsKey, payload)

    if (isResponseOk(res)) {
      yield put(actions.sendPaymentSmsKeySuccess())

      if (get(action, 'payload.isResending')) {
        yield call(resendPaymentSmsKeySaga)
      }
    } else {
      const data = getResponseData(res)
      const errorMessage =
        data && data.code === 'TRANSACTION_MISSING'
          ? Errors.TRANSACTION_MISSING
          : Errors.GENERAL_TECHNICAL_ERROR
      yield put(actions.sendPaymentSmsKeyFailure(errorMessage))
      yield put(push(Routes.ERROR))
    }
  } catch (e) {
    console.error(e) // eslint-disable-line
    yield put(actions.sendPaymentSmsKeyFailure())
    yield put(push(Routes.ERROR))
  }
}

export function* resendPaymentSmsKeySaga(): Saga {
  yield put(actions.setPaymentSmsResendMessageVisible(true))
  yield call(delay, 5000)
  yield put(actions.setPaymentSmsResendMessageVisible(false))
}

export function* verifyPaymentSmsKeySaga(api: Api): Saga {
  const payload = yield select(selectors.getVerifyPaymentSmsPayload)

  try {
    const res = yield call(api.verifyPaymentSmsKey, payload)
    const data = getResponseData(res)
    const code = get(data, 'code')

    if (isResponseOk(res)) {
      yield put(actions.verifyPaymentSmsKeySuccess())
      yield put(push(Routes.SUCCESS))
    } else if (code === 'BAD_OTP') {
      yield put(actions.verifyPaymentSmsKeyFailure(Errors.VERIFY_SMS_KEY_INCORRECT))
    } else {
      const errorMessage =
        code === 'TRANSACTION_MISSING' ? Errors.TRANSACTION_MISSING : Errors.GENERAL_TECHNICAL_ERROR
      yield put(actions.verifyPaymentSmsKeyFailure(errorMessage))
      yield put(push(Routes.ERROR))
    }
  } catch (e) {
    console.error(e) // eslint-disable-line
    yield put(push(Routes.ERROR))
  }
}
