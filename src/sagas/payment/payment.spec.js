import { call, select } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { expectSaga, testSaga } from 'redux-saga-test-plan'
import { push } from 'react-router-redux'

import MockApi from '../../api/mockApi'
import { Routes, Errors } from '../../constants'
import { paymentActions as actions, selectors } from '../../redux'
import { mockAction, mockResponse } from '../../utils'

import {
  getPaymentDataSaga,
  getAccountBalanceSaga,
  sendPaymentSmsKeySaga,
  resendPaymentSmsKeySaga,
  verifyPaymentSmsKeySaga,
} from './payment'

// These values must not change, else mocks won't work
const PAYMENT_ID = '123'
const SMS_KEY = '12341234'
const ACCOUNT_ID = '021452099'

describe('getPaymentDataSaga', () => {
  const payment_id = PAYMENT_ID // eslint-disable-line camelcase
  const mocks = response => [
    [select(selectors.getPaymentId), payment_id], // eslint-disable-line camelcase
    [call(MockApi.getPaymentData, payment_id), response],
  ]

  it('handles POWER_AUTH_SIGNATURE_INVALID error', () => {
    class ResponseError extends Error {
      constructor(message) {
        super(message)

        this.response = mockResponse('ERROR', { message: 'POWER_AUTH_SIGNATURE_INVALID' })
      }
    }

    testSaga(getPaymentDataSaga, MockApi)
      .next()
      .next()
      .throw(new ResponseError())
      .put(push(Routes.LOGIN))
  })

  it('handles OK response with valid instructionStatus', () => {
    const paymentData = { instructionStatus: 'FOR_SIGNING' }
    const response = mockResponse('OK', paymentData)

    return expectSaga(getPaymentDataSaga, MockApi)
      .provide(mocks(response))
      .put(actions.getPaymentDataSuccess(paymentData))
      .call(getAccountBalanceSaga, MockApi)
      .run()
  })

  it('handles OK response with invalid instructionStatus', () => {
    const paymentData = { instructionStatus: 'Gibberish' }

    const response = mockResponse('OK', paymentData)

    return expectSaga(getPaymentDataSaga, MockApi)
      .provide(mocks(response))
      .put(actions.getPaymentDataFailure(Errors.GET_PAYMENT_DATA_FAILURE))
      .put(push(Routes.ERROR))
      .run()
  })

  it('handles ERROR response with status code TRANSACTION_MISSING', () => {
    const response = mockResponse('ERROR', { code: 'TRANSACTION_MISSING' })

    return expectSaga(getPaymentDataSaga, MockApi)
      .provide(mocks(response))
      .put(actions.getPaymentDataFailure(Errors.TRANSACTION_MISSING))
      .put(push(Routes.ERROR))
      .run()
  })

  it('handles ERROR response', () => {
    const response = mockResponse('ERROR')

    return expectSaga(getPaymentDataSaga, MockApi)
      .provide(mocks(response))
      .put(actions.getPaymentDataFailure(Errors.GET_PAYMENT_DATA_FAILURE))
      .put(push(Routes.ERROR))
      .run()
  })
})

describe('getAccountBalanceSaga', () => {
  const account_id = ACCOUNT_ID // eslint-disable-line camelcase
  const payload = { account_id }
  const currency = 'CZK'

  const mocks = response => [
    [select(selectors.getAccountId), account_id], // eslint-disable-line camelcase
    [select(selectors.getAccountCurrency), currency], // eslint-disable-line camelcase
    [call(MockApi.getAccountBalance, account_id, currency), response],
  ]

  it('calls getAccountBalance endpoint with correct payload', () => {
    testSaga(getAccountBalanceSaga, MockApi)
      .next()
      .select(selectors.getAccountId)
      .next(payload)
      .select(selectors.getAccountCurrency)
      .next(currency)
      .call(MockApi.getAccountBalance, payload, currency)
  })

  it('handles thrown errors', () => {
    testSaga(getAccountBalanceSaga, MockApi)
      .next()
      .next()
      .next()
      .next()
      .throw()
      .put(actions.getAccountBalanceFailure())
      .next()
      .put(push(Routes.ERROR))
  })

  it('handles OK response', () => {
    const accountBalanceData = { foo: 'bar' }
    const response = mockResponse('OK', accountBalanceData)

    return expectSaga(getAccountBalanceSaga, MockApi)
      .provide(mocks(response))
      .put(actions.getAccountBalanceSuccess(accountBalanceData))
      .run()
  })

  it('handles ERROR response', () => {
    const response = mockResponse('ERROR')

    return expectSaga(getAccountBalanceSaga, MockApi)
      .provide(mocks(response))
      .put(actions.getAccountBalanceFailure())
      .put(push(Routes.ERROR))
      .run()
  })
})

describe('sendPaymentSmsKeySaga', () => {
  const payment_id = PAYMENT_ID // eslint-disable-line camelcase
  const mocks = response => [
    [select(selectors.getPaymentId), payment_id], // eslint-disable-line camelcase
    [call(MockApi.sendPaymentSmsKey, payment_id), response],
  ]

  it('calls correct endpoint with payload', () => {
    testSaga(sendPaymentSmsKeySaga, MockApi)
      .next()
      .select(selectors.getPaymentId)
      .next(payment_id)
      .call(MockApi.sendPaymentSmsKey, payment_id)
  })

  it('handles thrown errors', () => {
    testSaga(sendPaymentSmsKeySaga, MockApi)
      .next()
      .next()
      .throw()
      .put(actions.sendPaymentSmsKeyFailure())
      .next()
      .put(push(Routes.ERROR))
  })

  it('handles OK response with status 200', () => {
    const response = { ...mockResponse('OK'), status: 200 }

    return expectSaga(sendPaymentSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(actions.sendPaymentSmsKeySuccess())
      .run()
  })

  it('handles OK response with status 200 when isResending', () => {
    const action = mockAction({ isResending: true })
    const response = { ...mockResponse('OK'), status: 200 }

    return expectSaga(sendPaymentSmsKeySaga, MockApi, action)
      .provide(mocks(response))
      .put(actions.sendPaymentSmsKeySuccess())
      .call(resendPaymentSmsKeySaga)
      .run()
  })

  it('handles ERROR response with code TRANSACTION_MISSING', () => {
    const response = mockResponse('ERROR', { code: 'TRANSACTION_MISSING' })

    return expectSaga(sendPaymentSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(actions.sendPaymentSmsKeyFailure(Errors.TRANSACTION_MISSING))
      .put(push(Routes.ERROR))
      .run()
  })

  it('handles ERROR response', () => {
    const response = mockResponse('ERROR')

    return expectSaga(sendPaymentSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(actions.sendPaymentSmsKeyFailure(Errors.GENERAL_TECHNICAL_ERROR))
      .put(push(Routes.ERROR))
      .run()
  })
})

/**
 * Resend payment sms key
 */
describe('resendSmsKeySaga', () => {
  it('show/hides resend message', () => {
    testSaga(resendPaymentSmsKeySaga, MockApi)
      .next()
      .put(actions.setPaymentSmsResendMessageVisible(true))
      .next()
      .call(delay, 5000)
      .next()
      .put(actions.setPaymentSmsResendMessageVisible(false))
  })
})

/**
 * Verify payment sms
 */
describe('verifyPaymentSmsKeySaga', () => {
  const payload = { paymentId: PAYMENT_ID, smsKey: SMS_KEY }
  const mocks = response => [
    [select(selectors.getVerifyPaymentSmsPayload), payload],
    [call(MockApi.verifyPaymentSmsKey, payload), response],
  ]

  it('calls correct endpoint with payload', () => {
    testSaga(verifyPaymentSmsKeySaga, MockApi)
      .next()
      .select(selectors.getVerifyPaymentSmsPayload)
      .next(payload)
      .call(MockApi.verifyPaymentSmsKey, payload)
  })

  it('handles thrown errors', () => {
    testSaga(verifyPaymentSmsKeySaga, MockApi)
      .next()
      .next()
      .throw()
      .put(push(Routes.ERROR))
  })

  it('handles OK status', () => {
    const response = mockResponse('OK', { code: 'OK' })

    return expectSaga(verifyPaymentSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(actions.verifyPaymentSmsKeySuccess())
      .run()
  })

  it('handles ID_NOT_VALID code', () => {
    const response = mockResponse('ERROR', { code: 'ID_NOT_VALID' })

    return expectSaga(verifyPaymentSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(push(Routes.ERROR))
      .run()
  })

  it('handles TRANSACTION_MISSING code', () => {
    const response = mockResponse('ERROR', { code: 'TRANSACTION_MISSING' })

    return expectSaga(verifyPaymentSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(push(Routes.ERROR))
      .run()
  })

  it('handles BAD_OTP code', () => {
    const response = mockResponse('ERROR', { code: 'BAD_OTP' })

    return expectSaga(verifyPaymentSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(actions.verifyPaymentSmsKeyFailure(Errors.VERIFY_SMS_KEY_INCORRECT))
      .run()
  })

  it('handles ERROR with TRANSACTION_MISSING response', () => {
    const response = mockResponse('ERROR', { code: 'TRANSACTION_MISSING' })

    return expectSaga(verifyPaymentSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(actions.verifyPaymentSmsKeyFailure(Errors.TRANSACTION_MISSING))
      .put(push(Routes.ERROR))
      .run()
  })

  it('handles general ERROR response', () => {
    const response = mockResponse('ERROR')

    return expectSaga(verifyPaymentSmsKeySaga, MockApi)
      .provide(mocks(response))
      .put(actions.verifyPaymentSmsKeyFailure(Errors.GENERAL_TECHNICAL_ERROR))
      .put(push(Routes.ERROR))
      .run()
  })
})
