import { expectSaga } from 'redux-saga-test-plan'
import { push } from 'react-router-redux'

import MockApi from '../../api/mockApi'
import { Routes, AppModes } from '../../constants/'
import { appActions, paymentActions } from '../../redux'
import { mockAction } from '../../utils'

import { initialize } from './app'

describe('initialize', () => {
  it('exits when return_url is missing', () => {
    const action = mockAction({ return_url: undefined })

    return expectSaga(initialize, MockApi, action)
      .put(push(Routes.ERROR))
      .run()
  })

  it('exits when has return_url but no scope', () => {
    const action = mockAction({ return_url: 'foobar' })

    return expectSaga(initialize, MockApi, action)
      .put(push(Routes.ERROR))
      .run()
  })

  it('exits when unknown scope', () => {
    const scope = 'AYYLMAO'
    const action = mockAction({ return_url: 'foobar', client_name: 'foo', state: 'foo', scope })

    return expectSaga(initialize, MockApi, action)
      .put(push(Routes.ERROR))
      .run()
  })

  it('sets PAYMENT mode and calls getPaymentData when payment_id is present', () => {
    const action = mockAction({ return_url: 'foobar', payment_id: '123' })

    return expectSaga(initialize, MockApi, action)
      .put(appActions.setMode(AppModes.PAYMENT))
      .put(paymentActions.getPaymentData())
      .run()
  })

  it('sets IDENTITY_CONFIRMATION mode when KYC scope', () => {
    const scope = 'KYC'
    const action = mockAction({ return_url: 'foobar', client_name: 'foo', state: 'foo', scope })

    return expectSaga(initialize, MockApi, action)
      .put(appActions.setMode(AppModes.IDENTITY_CONFIRMATION))
      .run()
  })

  it('sets TOKEN mode when any other scope', () => {
    const scope = 'ALL'
    const action = mockAction({ return_url: 'foobar', client_name: 'foo', state: 'foo', scope })

    return expectSaga(initialize, MockApi, action)
      .put(appActions.setMode(AppModes.TOKEN))
      .run()
  })
})
