// @flow

/* eslint-disable camelcase */

import { call, put, select } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import { getMode, AppModes, Routes } from '../../constants/'
import { appActions, paymentActions, tokenActions, selectors } from '../../redux/'
import type { Api, Action, Saga } from '../../types/'

/**
 * The application handles various grant scenarios depending on the query input.
 * We process it on the initial render and set the app mode based on it.
 */
export function* initialize(api: Api, action: Action): Saga {
  const queryData = action.payload
  const { payment_id, client_name, return_url, state, scope } = queryData

  const usePaymentFlow = payment_id
  const useScopeBasedFlow = client_name && state && scope
  const isQueryValid = return_url && (usePaymentFlow || useScopeBasedFlow)

  yield put(appActions.putQueryData(queryData))

  if (!isQueryValid) {
    yield put(push(Routes.ERROR))
  } else if (usePaymentFlow) {
    yield call(handlePaymentFlow)
  } else if (useScopeBasedFlow) {
    yield call(handleScopeBasedFlow, scope)
  }
}

export function* handlePaymentFlow(): Saga {
  yield put(appActions.setMode(AppModes.PAYMENT))
  yield put(appActions.initializeSuccess())
  yield put(paymentActions.getPaymentData())
}

export function* handleScopeBasedFlow(scope: string): Saga {
  const mode = getMode(scope)

  if (!mode) yield put(push(Routes.ERROR))

  yield put(appActions.setMode(mode))
  yield put(appActions.initializeSuccess())
}

/**
 * Execute scenario specific successful verification logic.
 */
export function* handleVerificationSuccessSaga(): Saga {
  const usePaymentFlow = yield select(selectors.getUsePaymentFlow)
  const useIDConfirmFlow = yield select(selectors.getUseIDConfirmFlow)

  if (usePaymentFlow) {
    yield put(paymentActions.getPaymentData())
    yield put(push(Routes.PAYMENT))
  } else if (useIDConfirmFlow) {
    yield put(tokenActions.generateToken())
  } else {
    yield put(tokenActions.generateToken())
  }
}

export function* returnToOriginSaga(api: Api, action: Action): Saga {
  const { return_url, token, state } = yield select(selectors.getReturnToOriginProps)
  const { isError } = action.payload || {}

  const withResCode = isError ? `response_code=ERROR` : 'response_code=OK'
  const withToken = token ? `&federated_login_token=${token}` : ''
  const withState = state ? `&state=${state}` : ''

  // Redirect
  window.location = `${return_url}?${withResCode}${withToken}${withState}`
}
