// @flow

// All API related custom types should be located here: request and response shapes, etc.
export type Api = {
  login: (data: Object) => mixed,
  sendSmsKey: () => mixed,
  verifySmsKey: (data: Object) => mixed,
  generateToken: () => mixed,
  getPaymentData: (paymentId: string) => mixed,
  sendPaymentSmsKey: (paymentId: string) => mixed,
  verifyPaymentSmsKey: (payload: { paymentId: string, smsKey: string }) => mixed,
  getAccountBalance: (accountId: string, currency: string) => mixed,
  grantScopedConsent: (data: Object) => mixed,
}
