// @flow

import type { Saga as SagaType } from 'redux-saga'
import type { FormProps } from 'redux-form'
import type { MessageDescriptor } from 'react-intl'

// General Redux types
export type Action = {
  type: string,
  payload: any,
  meta?: any,
}

export type Dispatch = (action: Action) => void

export type Saga = SagaType<*>

type ErrorMessage = MessageDescriptor | string | null

// State shapes
export type AppState = {
  mode: string,
  queryData: Object,
  initialized: boolean,
}

export type AuthState = {
  isLoggingIn: boolean,
  isLoggedIn: boolean,
  loginError: ErrorMessage,

  isSendingSms: boolean,
  isSmsResendMessageVisible: boolean,

  isVerifying: boolean,
  isVerified: boolean,
  verifyError: ErrorMessage,
}

export type ConfigState = {
  apiUrl: string,
  environment: string,
  lang: string,
}

export type FederatedTokenState = {
  token: string,
  tokenError: ErrorMessage,
}

export type PaymentState = {
  paymentData: Object | null,
  paymentDataError: ErrorMessage,
  isPaymentDataLoading: boolean,
  isPaymentDataLoaded: boolean,

  accountBalanceData: Object | null,
  accountBalanceError: ErrorMessage,
  isAccountBalanceLoading: boolean,
  isAccountBalanceLoaded: boolean,

  isSmsSending: boolean,
  isSmsSent: boolean,
  sendSmsError: ErrorMessage,
  isPaymentSmsResendMessageVisible: boolean,

  isSmsVerifying: boolean,
  isSmsVerified: boolean,
  verifySmsError: ErrorMessage,
}

// Main state shape
export type State = {
  app: AppState,
  auth: AuthState,
  config: ConfigState,
  federatedToken: FederatedTokenState,
  payment: PaymentState,
  form: {
    login: FormProps,
    verifySms: FormProps,
    verifyPaymentSms: FormProps,
  },
}
