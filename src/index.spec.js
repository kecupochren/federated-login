import React from 'react'
import ReactDOM from 'react-dom'

import { render } from './index'

describe('render()', () => {
  it('renders the application', () => {
    ReactDOM.render = jest.fn()

    render(() => <div>Hey</div>)

    expect(ReactDOM.render).toBeCalled()
  })
})
