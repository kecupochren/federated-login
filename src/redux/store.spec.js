import { initStore } from './store'

describe('store()', () => {
  it('returns valid store', () => {
    const store = initStore()

    expect(store).toBeDefined()
  })
})
