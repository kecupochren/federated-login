// @flow

import { createActions, handleActions } from 'redux-actions'

import { Errors } from '../../constants'

import initialState from './model'

export const authActions = createActions(
  'LOGIN',
  'LOGIN_SUCCESS',
  'LOGIN_FAILURE',
  'LOGIN_RESET',
  'CLEAR_LOGIN_ERROR',

  'SEND_SMS_KEY',
  'SEND_SMS_KEY_SUCCESS',
  'SEND_SMS_KEY_FAILURE',
  'SET_SMS_RESEND_MESSAGE_VISIBLE',

  'VERIFY_SMS_KEY',
  'VERIFY_SMS_KEY_SUCCESS',
  'VERIFY_SMS_KEY_FAILURE',
  'CLEAR_VERIFY_ERROR'
)

export default handleActions(
  {
    // Login
    LOGIN: state =>
      state.merge({
        isLoggingIn: true,
        isLoggedIn: false,
        loginError: null,
      }),
    LOGIN_FAILURE: state =>
      state.merge({
        isLoggingIn: false,
        loginError: Errors.LOGIN_FAILURE,
      }),
    LOGIN_RESET: state =>
      state.merge({
        isLoggedIn: false,
      }),
    CLEAR_LOGIN_ERROR: state =>
      state.merge({
        loginError: null,
      }),

    // Send sms key
    SEND_SMS_KEY: state =>
      state.merge({
        isSendingSms: true,
        verifyError: null,
      }),
    SEND_SMS_KEY_SUCCESS: state =>
      state.merge({
        isSendingSms: false,
        isLoggingIn: false,
        isLoggedIn: true,
      }),
    SEND_SMS_KEY_FAILURE: (state, action) =>
      state.merge({
        isSendingSms: false,
        isLoggingIn: false,
        loginError: action.payload,
      }),
    SET_SMS_RESEND_MESSAGE_VISIBLE: (state, action) =>
      state.merge({
        isSmsResendMessageVisible: action.payload,
      }),

    // Verify sms key
    VERIFY_SMS_KEY: state =>
      state.merge({
        isVerifying: true,
        verifyError: null,
      }),
    VERIFY_SMS_KEY_SUCCESS: state =>
      state.merge({
        isVerified: true,
        isVerifying: false,
      }),
    VERIFY_SMS_KEY_FAILURE: (state, action) =>
      state.merge({
        isVerifying: false,
        verifyError: action.payload,
      }),
    CLEAR_VERIFY_ERROR: state =>
      state.merge({
        verifyError: null,
      }),
  },
  initialState
)
