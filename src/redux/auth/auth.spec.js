import { Errors } from '../../constants'

import initialState from './model'
import reducer, { authActions as actions } from './auth'

describe('login actions', () => {
  it('handles login ', () => {
    const state = reducer(initialState, actions.login())

    expect(state.isLoggingIn).toEqual(true)
    expect(state.isLoggedIn).toEqual(false)
    expect(state.loginError).toEqual(null)
  })

  it('handles login failure', () => {
    const state = reducer(initialState, actions.loginFailure())

    expect(state.isLoggingIn).toEqual(false)
    expect(state.loginError).toEqual(Errors.LOGIN_FAILURE)
  })

  it('handles login reset', () => {
    const state = reducer(initialState, actions.loginReset())

    expect(state.isLoggedIn).toEqual(false)
  })

  it('clears login error', () => {
    const state = reducer(initialState, actions.clearLoginError())

    expect(state.loginError).toEqual(null)
  })
})

// The login process finishes by sending the verifying sms
describe('sendSmsKey actions', () => {
  it('handles sendSmsKey', () => {
    const state = reducer(initialState, actions.sendSmsKey())

    expect(state.verifyError).toEqual(null)
    expect(state.isSendingSms).toEqual(true)
  })

  it('handles sendSmsKey success', () => {
    const state = reducer(initialState, actions.sendSmsKeySuccess())

    expect(state.isLoggingIn).toEqual(false)
    expect(state.isLoggedIn).toEqual(true)
    expect(state.isSendingSms).toEqual(false)
  })

  it('handles sendSmsKey failure', () => {
    const error = { id: 'error.foobar' }
    const state = reducer(initialState, actions.sendSmsKeyFailure(error))

    expect(state.isLoggingIn).toEqual(false)
    expect(state.isSendingSms).toEqual(false)
    expect(state.loginError).toEqual(error)
  })

  it('clears login error', () => {
    const prevState = reducer(initialState, actions.sendSmsKeyFailure({ foo: 'bar' }))
    const state = reducer(prevState, actions.clearLoginError())

    expect(state.loginError).toEqual(null)
  })
})

describe('verifySmsKey actions', () => {
  it('handles verifySmsKey', () => {
    const state = reducer(initialState, actions.verifySmsKey())

    expect(state.isVerifying).toEqual(true)
    expect(state.verifyError).toEqual(null)
  })

  it('handles verifySmsKey success', () => {
    const state = reducer(initialState, actions.verifySmsKeySuccess())

    expect(state.isVerified).toEqual(true)
    expect(state.isVerifying).toEqual(false)
  })

  it('handles verifySmsKey failure', () => {
    const error = { id: 'error.verify_sms_key_incorrect' }
    const state = reducer(initialState, actions.verifySmsKeyFailure(error))

    expect(state.isVerifying).toEqual(false)
    expect(state.verifyError).toEqual(error)
  })

  it('clears verify error', () => {
    const prevState = reducer(initialState, actions.verifySmsKeyFailure({ foo: 'bar' }))
    const state = reducer(prevState, actions.clearVerifyError())

    expect(state.verifyError).toEqual(null)
  })
})
