// @flow

import Immutable from 'seamless-immutable'

import type { AuthState } from '../../types'

const state: AuthState = {
  isLoggingIn: false,
  isLoggedIn: false,
  loginError: null,

  isSendingSms: false,
  isSmsResendMessageVisible: false,

  isVerifying: false,
  isVerified: false,
  verifyError: null,
}

export default Immutable(state)
