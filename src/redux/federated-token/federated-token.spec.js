// @flow

import { Errors } from '../../constants'

import initialState from './model'
import reducer, { tokenActions as actions } from './federated-token'

describe('generateToken actions', () => {
  it('handles generateToken success', () => {
    const token = 'foobar'
    const state = reducer(initialState, actions.generateTokenSuccess(token))

    expect(state.token).toEqual(token)
  })

  it('handles generateToken failure', () => {
    const state = reducer(initialState, actions.generateTokenFailure())

    expect(state.tokenError).toEqual(Errors.GENERATE_TOKEN_FAILURE)
  })
})
