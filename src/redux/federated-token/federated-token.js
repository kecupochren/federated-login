// @flow

import { createActions, handleActions } from 'redux-actions'

import { Errors } from '../../constants'

import initialState from './model'

export const tokenActions = createActions(
  'GENERATE_TOKEN',
  'GENERATE_TOKEN_SUCCESS',
  'GENERATE_TOKEN_FAILURE'
)

export default handleActions(
  {
    // Generate federated token
    GENERATE_TOKEN_SUCCESS: (state, action) =>
      state.merge({
        token: action.payload,
      }),
    GENERATE_TOKEN_FAILURE: state =>
      state.merge({
        tokenError: Errors.GENERATE_TOKEN_FAILURE,
      }),
  },
  initialState
)
