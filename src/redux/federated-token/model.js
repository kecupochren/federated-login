// @flow

import Immutable from 'seamless-immutable'

import type { FederatedTokenState } from '../../types'

const state: FederatedTokenState = {
  token: '',
  tokenError: null,
}

export default Immutable(state)
