import { Environments } from '../../constants'

import reducer, { configActions as actions } from './config'
import initialState from './model'

describe('config reducer', () => {
  it('returns the initial state', () => {
    const state = reducer(initialState, {})

    expect(state).toEqual(initialState)
  })

  describe('SET_ENVIRONMENT actions', () => {
    const defaultEnv = {
      apiUrl: Environments.DEV.apiUrl,
      environment: 'DEV',
      lang: 'cs',
    }

    it('handles setEnvironment with existing env', () => {
      const state = reducer(initialState, actions.setEnvironment('DEV'))

      expect(state).toEqual(defaultEnv)
    })

    it('handles setEnvironment with undefined env', () => {
      const state = reducer(initialState, actions.setEnvironment(undefined))

      expect(state).toEqual(defaultEnv)
    })

    it('handles setEnvironment with non-existing env', () => {
      const state = reducer(initialState, actions.setEnvironment('Foobar'))

      expect(state).toEqual(defaultEnv)
    })
  })

  describe('SET_LANGUAGE actions', () => {
    it('handles setLanguage', () => {
      const lang = 'en'
      const state = reducer(initialState, actions.setLanguage(lang))

      expect(state.lang).toEqual(lang)
    })
  })
})
