// @flow

import { createActions, handleActions } from 'redux-actions'
import store from 'store'

import { Environments } from '../../constants'

import initialState from './model'

export const configActions = createActions('SET_ENVIRONMENT', 'SET_LANGUAGE')

export default handleActions(
  {
    SET_ENVIRONMENT: (state, action) => {
      const env = action.payload
      const config = Environments[env] || Environments.DEV

      if (Environments[env]) {
        store.set('env', env)
      }

      return state.merge({
        apiUrl: config.apiUrl,
        environment: Environments[env] ? env : 'DEV',
      })
    },

    SET_LANGUAGE: (state, action) =>
      state.merge({
        lang: action.payload,
      }),
  },
  initialState
)
