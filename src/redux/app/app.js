// @flow

import { createActions, handleActions } from 'redux-actions'

import initialState from './model'

export const appActions = createActions(
  'SET_MODE',

  'PUT_QUERY_DATA',

  'INITIALIZE',
  'INITIALIZE_SUCCESS',

  'RETURN_TO_ORIGIN'
)

export default handleActions(
  {
    SET_MODE: (state, action) =>
      state.merge({
        mode: action.payload,
      }),

    PUT_QUERY_DATA: (state, action) =>
      state.merge({
        queryData: action.payload,
      }),

    INITIALIZE_SUCCESS: state =>
      state.merge({
        initialized: true,
      }),
  },
  initialState
)
