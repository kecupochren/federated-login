// @flow

import Immutable from 'seamless-immutable'

import { AppModes } from '../../constants/'
import type { AppState } from '../../types'

const state: AppState = {
  mode: AppModes.TOKEN,
  queryData: {},
  initialized: false,
}

export default Immutable(state)
