import initialState from './model'
import reducer, { appActions } from './app'

describe('setMode', () => {
  it('sets mode', () => {
    const mode = 'foo'
    const state = reducer(initialState, appActions.setMode(mode))

    expect(state.mode).toEqual(mode)
  })
})

describe('initializeSuccess', () => {
  it('sets initialized to true', () => {
    const state = reducer(initialState, appActions.initializeSuccess())

    expect(state.initialized).toBe(true)
  })
})

describe('putQueryData', () => {
  it('saves query data', () => {
    const queryData = { foo: 'bar' }
    const state = reducer(initialState, appActions.putQueryData(queryData))

    expect(state.queryData).toEqual(queryData)
  })
})
