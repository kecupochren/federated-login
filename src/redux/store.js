// @flow

import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import { reducer as formReducer } from 'redux-form'
// import logger from '@prague-digi/redux-logger'
import createSagaMiddleware from 'redux-saga'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createHashHistory'

import app from './app'
import auth from './auth'
import config from './config'
import federatedToken from './federated-token'
import payment from './payment'
import rootSaga from '../sagas'

export const history = createHistory()

export const reducer = combineReducers({
  app,
  auth,
  config,
  federatedToken,
  payment,
  form: formReducer,
  router: routerReducer,
})

// eslint-disable-next-line
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const initStore = () => {
  const middleware = []
  const sagaMiddleware = createSagaMiddleware()

  middleware.push(sagaMiddleware)
  middleware.push(routerMiddleware(history))
  // middleware.push(logger)

  const store = createStore(reducer, composeEnhancers(applyMiddleware(...middleware)))

  // sagaMiddleware is exposed because of our saga HMR
  // $FlowFixMe
  store.sagaMiddleware = sagaMiddleware
  sagaMiddleware.run(rootSaga)

  return store
}

export default initStore()
