// @flow

import { Errors } from '../../constants'

import initialState from './model'
import reducer, { paymentActions as actions } from './payment'

describe('getPayment actions', () => {
  it('handles getPayment', () => {
    const state = reducer(initialState, actions.getPaymentData())

    expect(state.isPaymentDataLoading).toEqual(true)
  })

  it('handles getPayment success', () => {
    const data = { foo: 'bar' }
    const state = reducer(initialState, actions.getPaymentDataSuccess(data))

    expect(state.isPaymentDataLoading).toEqual(false)
    expect(state.isPaymentDataLoaded).toEqual(true)
    expect(state.paymentData).toEqual(data)
  })

  it('handles getPayment failure', () => {
    const state = reducer(
      initialState,
      actions.getPaymentDataFailure(Errors.GET_PAYMENT_DATA_FAILURE)
    )

    expect(state.isPaymentDataLoading).toEqual(false)
    expect(state.paymentDataError).toEqual(Errors.GET_PAYMENT_DATA_FAILURE)
  })
})

describe('getAccountBalance actions', () => {
  it('handles getAccountBalance', () => {
    const state = reducer(initialState, actions.getAccountBalance())

    expect(state.isAccountBalanceLoading).toEqual(true)
  })

  it('handles getAccountBalance success', () => {
    const data = { foo: 'bar' }
    const state = reducer(initialState, actions.getAccountBalanceSuccess(data))

    expect(state.isAccountBalanceLoading).toEqual(false)
    expect(state.isAccountBalanceLoaded).toEqual(true)
    expect(state.accountBalanceData).toEqual(data)
  })

  it('handles getAccountBalance failure', () => {
    const state = reducer(initialState, actions.getAccountBalanceFailure())

    expect(state.isAccountBalanceLoading).toEqual(false)
    expect(state.accountBalanceError).toEqual(Errors.GET_ACCOUNT_BALANCE_FAILURE)
  })
})

describe('sendPaymentSmsKey actions', () => {
  it('handles sendPaymentSmsKey', () => {
    const state = reducer(initialState, actions.sendPaymentSmsKey())

    expect(state.isSmsSending).toEqual(true)
  })

  it('handles sendPaymentSmsKeySuccess', () => {
    const state = reducer(initialState, actions.sendPaymentSmsKeySuccess())

    expect(state.isSmsSending).toEqual(false)
    expect(state.isSmsSent).toEqual(true)
  })

  it('handles sendPaymentSmsKeyFailure', () => {
    const error = { id: 'error.foobar' }
    const state = reducer(initialState, actions.sendPaymentSmsKeyFailure(error))

    expect(state.isSmsSending).toEqual(false)
    expect(state.sendSmsError).toEqual(error)
  })
})

describe('verifyPaymentSmsKey actions', () => {
  it('handles verifyPaymentSmsKey', () => {
    const state = reducer(initialState, actions.verifyPaymentSmsKey())

    expect(state.isSmsVerifying).toEqual(true)
    expect(state.verifySmsError).toEqual(null)
  })

  it('handles verifyPaymentSmsKeySuccess', () => {
    const state = reducer(initialState, actions.verifyPaymentSmsKeySuccess())

    expect(state.isSmsVerifying).toEqual(false)
    expect(state.isSmsVerified).toEqual(true)
  })

  it('handles verifyPaymentSmsKeyFailure', () => {
    const error = { id: 'error.foobar' }
    const state = reducer(initialState, actions.verifyPaymentSmsKeyFailure(error))

    expect(state.isSmsVerifying).toEqual(false)
    expect(state.verifySmsError).toEqual(error)
  })

  it('clears verify error', () => {
    const prevState = reducer(initialState, actions.verifyPaymentSmsKeyFailure({ foo: 'bar' }))
    const state = reducer(prevState, actions.clearPaymentVerifyError())

    expect(state.verifySmsError).toEqual(null)
  })
})
