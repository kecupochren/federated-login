// @flow

import { createActions, handleActions } from 'redux-actions'

import { Errors } from '../../constants'

import initialState from './model'

export const paymentActions = createActions(
  'GET_PAYMENT_DATA',
  'GET_PAYMENT_DATA_SUCCESS',
  'GET_PAYMENT_DATA_FAILURE',

  'GET_ACCOUNT_BALANCE',
  'GET_ACCOUNT_BALANCE_SUCCESS',
  'GET_ACCOUNT_BALANCE_FAILURE',

  'SEND_PAYMENT_SMS_KEY',
  'SEND_PAYMENT_SMS_KEY_SUCCESS',
  'SEND_PAYMENT_SMS_KEY_FAILURE',
  'SET_PAYMENT_SMS_RESEND_MESSAGE_VISIBLE',

  'VERIFY_PAYMENT_SMS_KEY',
  'VERIFY_PAYMENT_SMS_KEY_SUCCESS',
  'VERIFY_PAYMENT_SMS_KEY_FAILURE',
  'CLEAR_PAYMENT_VERIFY_ERROR'
)

export default handleActions(
  {
    // Get payment data
    GET_PAYMENT_DATA: state =>
      state.merge({
        isPaymentDataLoading: true,
      }),
    GET_PAYMENT_DATA_SUCCESS: (state, action) =>
      state.merge({
        paymentData: action.payload,
        isPaymentDataLoading: false,
        isPaymentDataLoaded: true,
      }),
    GET_PAYMENT_DATA_FAILURE: (state, action) =>
      state.merge({
        isPaymentDataLoading: false,
        paymentDataError: action.payload,
      }),

    // Get account balance
    GET_ACCOUNT_BALANCE: state =>
      state.merge({
        isAccountBalanceLoading: true,
      }),
    GET_ACCOUNT_BALANCE_SUCCESS: (state, action) =>
      state.merge({
        accountBalanceData: action.payload,
        isAccountBalanceLoading: false,
        isAccountBalanceLoaded: true,
      }),
    GET_ACCOUNT_BALANCE_FAILURE: state =>
      state.merge({
        isAccountBalanceLoading: false,
        accountBalanceError: Errors.GET_ACCOUNT_BALANCE_FAILURE,
      }),

    // Send payment sms key
    SEND_PAYMENT_SMS_KEY: state =>
      state.merge({
        isSmsSending: true,
      }),
    SEND_PAYMENT_SMS_KEY_SUCCESS: state =>
      state.merge({
        isSmsSending: false,
        isSmsSent: true,
      }),
    SEND_PAYMENT_SMS_KEY_FAILURE: (state, action) =>
      state.merge({
        isSmsSending: false,
        isSmsSent: true,
        sendSmsError: action.payload,
      }),
    SET_PAYMENT_SMS_RESEND_MESSAGE_VISIBLE: (state, action) =>
      state.merge({
        isPaymentSmsResendMessageVisible: action.payload,
      }),

    // Verify payment sms key
    VERIFY_PAYMENT_SMS_KEY: state =>
      state.merge({
        isSmsVerifying: true,
        verifySmsError: null,
      }),
    VERIFY_PAYMENT_SMS_KEY_SUCCESS: state =>
      state.merge({
        isSmsVerifying: false,
        isSmsVerified: true,
      }),
    VERIFY_PAYMENT_SMS_KEY_FAILURE: (state, action) =>
      state.merge({
        isSmsVerifying: false,
        verifySmsError: action.payload,
      }),
    CLEAR_PAYMENT_VERIFY_ERROR: state =>
      state.merge({
        verifySmsError: null,
      }),
  },
  initialState
)
