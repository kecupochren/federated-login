// @flow

import Immutable from 'seamless-immutable'

import type { PaymentState } from '../../types'

const state: PaymentState = {
  paymentData: null,
  paymentDataError: null,
  isPaymentDataLoading: false,
  isPaymentDataLoaded: false,

  accountBalanceData: null,
  accountBalanceError: null,
  isAccountBalanceLoading: false,
  isAccountBalanceLoaded: false,

  isSmsSending: false,
  isSmsSent: false,
  sendSmsError: null,
  isPaymentSmsResendMessageVisible: false,

  isSmsVerifying: false,
  isSmsVerified: false,
  verifySmsError: null,
}

export default Immutable(state)
