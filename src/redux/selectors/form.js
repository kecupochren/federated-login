// @flow

import { Base64 } from 'js-base64'
import type { State } from '../../types'

export const getLoginValues = (state: State) => state.form.login.values

export const getLoginCredentials = (state: State) => {
  const { id, password } = getLoginValues(state)

  return {
    credentials: {
      username: id,
      authenticationToken: Base64.encode(password),
    },
  }
}

export const getSmsKey = (state: State) => state.form.verifySms.values.smsKey

export const getSmsKeyData = (state: State) => ({ mobileKey: getSmsKey(state) })

export const getPaymentSmsKey = (state: State) => state.form.verifyPaymentSms.values.smsKey
