import * as selectors from './config'
import type { State } from '../../types/'

describe('config selectors', () => {
  it('can select state', () => {
    expect(selectors.getConfig(({ config: { apiUrl: 'foo' } }: State))).toEqual({ apiUrl: 'foo' })
  })
})
