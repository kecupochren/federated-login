// @flow

import { AppModes } from '../../constants'
import type { State } from '../../types'

import { getFederatedToken } from './'

export const getQueryData = (state: State) => state.app.queryData

export const getUsePaymentFlow = (state: State) => state.app.mode === AppModes.PAYMENT

export const getUseIDConfirmFlow = (state: State) =>
  state.app.mode === AppModes.IDENTITY_CONFIRMATION

export const getReturnToOriginProps = (state: State) => ({
  ...state.app.queryData,
  token: getFederatedToken(state),
})
