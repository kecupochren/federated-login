/* eslint-disable camelcase */

import {
  getPaymentId,
  getVerifyPaymentSmsPayload,
  getAccountId,
  getAccountCurrency,
} from './payment'

const PAYMENT_ID = '123'
const SMS_KEY = '12341234'

describe('getPaymentId selector', () => {
  it('gets paymentId', () => {
    const payment_id = PAYMENT_ID
    const state = { app: { queryData: { payment_id } } }

    expect(getPaymentId(state)).toEqual(payment_id)
  })
})

describe('getVerifyPaymentSmsPayload selector', () => {
  it('gets payload for verifying payment sms', () => {
    const payment_id = PAYMENT_ID
    const smsKey = SMS_KEY
    const state = {
      form: { verifyPaymentSms: { values: { smsKey } } },
      app: { queryData: { payment_id } },
    }

    expect(getVerifyPaymentSmsPayload(state)).toEqual({
      paymentId: payment_id,
      smsKey,
    })
  })
})

describe('getAccountId selector', () => {
  it('gets account number without bank code', () => {
    const accountId = '021452099/0600'
    const state = {
      payment: {
        paymentData: {
          debtorAccount: {
            identification: {
              other: {
                identification: accountId,
              },
            },
          },
        },
      },
    }

    expect(getAccountId(state)).toEqual('021452099')
  })
})

describe('getAccountCurrency selector', () => {
  it('gets account currency', () => {
    const state = {
      payment: {
        paymentData: {
          debtorAccount: {
            currency: 'CZK',
          },
        },
      },
    }

    expect(getAccountCurrency(state)).toEqual('CZK')
  })
})
