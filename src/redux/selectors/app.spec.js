import { AppModes } from '../../constants'

import { getUsePaymentFlow } from './app'

describe('getUsePaymentFlow selector', () => {
  it('gets usePaymentFlow bool', () => {
    const state = { app: { mode: AppModes.PAYMENT } }

    expect(getUsePaymentFlow(state)).toEqual(true)
  })
})
