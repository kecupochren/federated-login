// @flow

import type { State } from '../../types'

import { getQueryData } from './'

export const getFederatedToken = (state: State) => state.federatedToken.token

export const getReturnUrlProps = (state: State) => ({
  ...getQueryData(state),
  token: getFederatedToken(state),
})
