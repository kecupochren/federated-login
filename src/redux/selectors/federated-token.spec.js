import { getReturnUrlProps } from './federated-token'

describe('getReturnUrlProps selector', () => {
  it('gets data needed for building the return url', () => {
    const token = 'foobar'
    const csrfToken = 'CSRF token'
    const return_url = 'http://www.google.com' // eslint-disable-line camelcase

    const state = {
      app: { queryData: { return_url, state: csrfToken } },
      federatedToken: { token },
    }

    expect(getReturnUrlProps(state)).toEqual({
      return_url,
      token,
      state: csrfToken,
    })
  })
})
