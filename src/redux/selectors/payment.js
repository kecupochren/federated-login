// @flow

import get from 'lodash/get'

import { getPaymentSmsKey, getQueryData } from './index'
import type { State } from '../../types/'

export const getPaymentId = (state: State) => get(getQueryData(state), 'payment_id')

export const getVerifyPaymentSmsPayload = (state: State) => ({
  paymentId: getPaymentId(state),
  smsKey: getPaymentSmsKey(state),
})

export const getAccountId = (state: State) => {
  const accountId = get(
    state.payment.paymentData,
    'debtorAccount.identification.other.identification'
  )

  return accountId.slice(0, -5)
}

export const getAccountCurrency = (state: State) =>
  get(state.payment.paymentData, 'debtorAccount.currency')
