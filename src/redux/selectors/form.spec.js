import { Base64 } from 'js-base64'
import { getLoginValues, getLoginCredentials, getSmsKey, getSmsKeyData } from './form'

const SMS_KEY = '12341234'

describe('getLoginValues selector', () => {
  it('gets login values', () => {
    const values = { id: '123', password: '456' }
    const state = { form: { login: { values } } }

    expect(getLoginValues(state)).toEqual(values)
  })
})

describe('getLoginCredentials selector', () => {
  it('gets transformed login values', () => {
    const values = { id: '123', password: '456' }
    const state = { form: { login: { values } } }

    expect(getLoginCredentials(state)).toEqual({
      credentials: {
        username: values.id,
        authenticationToken: Base64.encode(values.password),
      },
    })
  })
})

describe('getSmsKey selector', () => {
  it('gets sms key', () => {
    const smsKey = SMS_KEY
    const state = { form: { verifySms: { values: { smsKey } } } }

    expect(getSmsKey(state)).toEqual(smsKey)
  })
})

describe('getSmsKeyData selector', () => {
  it('gets sms key in format for the API', () => {
    const smsKey = SMS_KEY
    const state = { form: { verifySms: { values: { smsKey } } } }

    expect(getSmsKeyData(state)).toEqual({ mobileKey: smsKey })
  })
})
