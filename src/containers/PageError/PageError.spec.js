import React from 'react'
import { shallow } from 'enzyme'

import { PageError } from './PageError'

describe('<PageError />', () => {
  const props = {
    app: {
      queryData: {
        return_url: 'foobar',
      },
    },
    payment: {},
    returnToOrigin: jest.fn(),
  }

  describe('render', () => {
    describe('when has return_url', () => {
      it('matches snapshot', () => {
        const tree = shallow(<PageError {...props} />)

        expect(tree).toMatchSnapshot()
      })
    })

    describe('when has return_url and paymentDataError', () => {
      it('matches snapshot', () => {
        const payment = { paymentDataError: { id: 'foo' } }
        const tree = shallow(<PageError {...props} payment={payment} />)

        expect(tree).toMatchSnapshot()
      })
    })
  })

  describe('handleReturn', () => {
    it('calls returnToOrigin with isError', () => {
      const tree = shallow(<PageError {...props} />)

      tree.instance().handleReturn()

      expect(props.returnToOrigin).toBeCalledWith({ isError: true })
    })
  })
})
