// @flow

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { Errors } from '../../constants'
import { Page, Button, Text, ErrorIcon } from '../../components'
import { appActions } from '../../redux/'
import type { Action, State, AppState, PaymentState } from '../../types/'

type Props = {
  app: AppState,
  payment: PaymentState,
  returnToOrigin: (opts: { isError: boolean }) => Action,
}

export class PageError extends PureComponent<Props, *> {
  handleReturn = () => this.props.returnToOrigin({ isError: true })

  props: Props

  render() {
    const { app, payment }: Props = this.props

    const hasReturnUrl = Boolean(app.queryData.return_url)
    const errorMessage = payment.paymentDataError || Errors.GENERAL_TECHNICAL_ERROR

    return (
      <Page>
        <ErrorIcon />

        <Text center message={errorMessage} />

        {hasReturnUrl && (
          <Button
            center
            anchor
            primary
            type="button"
            onClick={this.handleReturn}
            message={{ id: 'button.return_to_vendor' }}
          />
        )}
      </Page>
    )
  }
}

const withStore = connect(
  ({ app, payment }: State) => ({
    app,
    payment,
  }),
  {
    returnToOrigin: appActions.returnToOrigin,
  }
)

export default withStore(PageError)
