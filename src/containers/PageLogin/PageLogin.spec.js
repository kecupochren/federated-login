import React from 'react'
import { shallow } from 'enzyme'

import { AppModes } from '../../constants/'

import { PageLogin } from './PageLogin'

describe('PageLogin', () => {
  const props = {
    app: {
      mode: AppModes.TOKEN,
      initialized: false,
      queryData: null,
    },
    initialize: jest.fn(),
    returnToOrigin: jest.fn(),
    auth: {
      isLoggedIn: false,
    },
  }

  it('renders not initialized correctly', () => {
    const tree = shallow(<PageLogin {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders initialized correctly', () => {
    const app = { initialized: true }
    const tree = shallow(<PageLogin {...props} app={app} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders initialized & confirmed correctly', () => {
    const app = { initialized: true }
    const tree = shallow(<PageLogin {...props} app={app} />)

    tree.instance().setState({ hasConfirmed: true })

    expect(tree).toMatchSnapshot()
  })

  it('renders initialized & confirmed & loggedIn correctly', () => {
    const auth = { isLoggedIn: true }
    const app = { initialized: true }
    const tree = shallow(<PageLogin {...props} auth={auth} app={app} />)

    tree.instance().setState({ hasConfirmed: true })

    expect(tree).toMatchSnapshot()
  })

  it('initializes with parsed query on mount', () => {
    const location = { search: '?foo=bar' }
    const tree = shallow(<PageLogin {...props} location={location} />)

    jest.useFakeTimers()
    tree.instance().componentDidMount()
    jest.runAllTimers()

    expect(props.initialize).toBeCalledWith({ foo: 'bar' })
  })

  it('handles confirm', () => {
    const tree = shallow(<PageLogin {...props} />)

    tree.instance().handleConfirm()

    expect(tree.state().hasConfirmed).toBe(true)
  })
})
