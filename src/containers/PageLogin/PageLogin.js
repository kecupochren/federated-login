// @flow

import React, { PureComponent } from 'react'
import get from 'lodash/get'
import { connect } from 'react-redux'
import qs from 'query-string'

import { AppModes } from '../../constants'
import { Page } from '../../components'
import { appActions } from '../../redux'
import type { Action, AppState, AuthState, State as StateType } from '../../types'

import { VerifySmsForm, LoginHeader, LoginForm, ConfirmForm, PermissionsList } from './components'

type Props = {
  app: AppState,
  auth: AuthState,
  initialize: (query: Object) => Action,
  returnToOrigin: (opts: { isError: boolean }) => Action,
  location: {
    search: string,
  },
}

type State = {
  hasConfirmed: boolean,
}

export class PageLogin extends PureComponent<Props, State> {
  static defaultProps = {
    location: { search: '' },
  }

  state: State = { hasConfirmed: false }

  componentDidMount() {
    const { app: { initialized }, location, initialize }: Props = this.props

    if (!initialized) {
      const query = qs.parse(location.search)

      setTimeout(() => initialize(query), 50)
    }
  }

  props: Props

  handleConfirm = () => this.setState({ hasConfirmed: true })

  handleReject = () => this.props.returnToOrigin({ isError: true })

  render = () => {
    const { hasConfirmed } = this.state
    const { app: { initialized, mode, queryData }, auth: { isLoggedIn } }: Props = this.props

    const usePaymentFlow = mode === AppModes.PAYMENT
    const isConfirmVisible = !hasConfirmed && !usePaymentFlow
    const hasReturnUrl = get(queryData, 'return_url')

    return (
      <Page>
        {initialized && (
          <div>
            <LoginHeader mode={mode} queryData={queryData} />

            {isConfirmVisible ? (
              <ConfirmForm
                mode={mode}
                handleConfirm={this.handleConfirm}
                handleReject={this.handleReject}
                hasReturnUrl={hasReturnUrl}
              />
            ) : (
              <div>
                <LoginForm />

                {isLoggedIn && <VerifySmsForm queryData={queryData} />}
              </div>
            )}

            {!usePaymentFlow && <PermissionsList queryData={queryData} />}
          </div>
        )}
      </Page>
    )
  }
}

// istanbul ignore next
const withStore = connect(
  ({ app, auth }: StateType) => ({
    app,
    auth,
  }),
  {
    initialize: appActions.initialize,
    returnToOrigin: appActions.returnToOrigin,
  }
)

export default withStore(PageLogin)
