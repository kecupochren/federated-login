// @flow

import React from 'react'
import get from 'lodash/get'

import { PermissionScopes as scopes } from '../../../../constants'
import { Text } from '../../../../components'

type Props = {
  queryData: ?Object,
}

export const PermissionsList = ({ queryData }: Props) => {
  const scope = get(queryData, 'scope')
  const application = get(queryData, 'client_name')
  const values = { application }
  const mode = scope === scopes.KYC ? '.KYC' : ''

  return (
    <div>
      <Text small message={{ id: `page.login.permissions.title${mode}` }} values={values} />

      {(scope === scopes.ALL || scope === scopes.AISP) && (
        <Text
          small
          testid="permission-account-info"
          message={{ id: `page.login.permissions.${scopes.AISP}` }}
          values={values}
        />
      )}

      {(scope === scopes.ALL || scope === scopes.PISP) && (
        <Text
          small
          testid="permission-payment-init"
          message={{ id: `page.login.permissions.${scopes.PISP}` }}
          values={values}
        />
      )}

      {scope === scopes.KYC && (
        <Text
          small
          testid="permission-identity-confirmation"
          message={{ id: `page.login.permissions.${scopes.KYC}` }}
          values={values}
        />
      )}

      <Text small message={{ id: 'page.login.permissions.note' }} values={values} />
    </div>
  )
}

export default PermissionsList
