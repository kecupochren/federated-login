import React from 'react'
import { shallow } from 'enzyme'
import { PermissionsList } from './PermissionsList'

describe('<PermissionsList />', () => {
  const props = {
    queryData: {
      client_name: 'Foo',
      scope: 'ALL',
    },
  }

  it('renders correctly', () => {
    const tree = shallow(<PermissionsList {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders ALL permissions', () => {
    const tree = shallow(<PermissionsList {...props} />)

    expect(tree.find('[testid="permission-account-info"]')).toHaveLength(1)
    expect(tree.find('[testid="permission-payment-init"]')).toHaveLength(1)
  })

  it('renders PISP permission', () => {
    const queryData = { scope: 'PISP' }
    const tree = shallow(<PermissionsList {...props} queryData={queryData} />)

    expect(tree.find('[testid="permission-account-info"]')).toHaveLength(0)
    expect(tree.find('[testid="permission-payment-init"]')).toHaveLength(1)
  })

  it('renders AISP permission', () => {
    const queryData = { scope: 'AISP' }
    const tree = shallow(<PermissionsList {...props} queryData={queryData} />)

    expect(tree.find('[testid="permission-account-info"]')).toHaveLength(1)
    expect(tree.find('[testid="permission-payment-init"]')).toHaveLength(0)
  })
})
