// @flow

import React from 'react'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import get from 'lodash/get'

import { AppModes, PermissionScopes as scopes } from '../../../../constants'
import { Button, Message, Input } from '../../../../components'
import { authActions as actions } from '../../../../redux'
import { required } from '../../../../utils/index'
import type { Action, Dispatch, AppState, AuthState, State } from '../../../../types/'

type Props = {
  app: AppState,
  auth: AuthState,
  valid: boolean,
  handleSubmit: () => mixed,
  sendSmsKey: (opts: Object) => Action,
  clearVerifyError: () => Action,
  queryData: ?Object,
}

export const VerifySmsForm = (props: Props) => {
  const { app, auth, valid, handleSubmit, clearVerifyError, sendSmsKey, queryData } = props
  const { verifyError, isVerifying, isSmsResendMessageVisible, isSendingSms } = auth
  const scope = get(queryData, 'scope')

  let submitLabel = 'button.grant_access'
  if (scope === scopes.KYC) {
    submitLabel = 'button.continue'
  } else if (app.mode === AppModes.PAYMENT) {
    submitLabel = 'button.verify_sms_key'
  }

  return (
    <form onSubmit={handleSubmit}>
      <Input
        name="smsKey"
        messages={{ label: { id: 'label.sms_key' } }}
        onChange={clearVerifyError}
      />

      <Message error message={verifyError} isVisible={verifyError} />

      <Button
        disabled={!valid}
        submitting={isVerifying}
        type="submit"
        message={{ id: submitLabel }}
        primary
      />

      <Message
        success
        message={{ id: 'success.resend_sms' }}
        isVisible={isSmsResendMessageVisible}
      />

      <Button
        disabled={isSendingSms}
        type="button"
        message={{ id: 'button.new_sms_key' }}
        anchor
        testid="btnResend"
        onClick={() => sendSmsKey({ isResending: true })}
      />
    </form>
  )
}

export const validate = ({ smsKey }: Object) => ({ smsKey: required(smsKey) })

export const onSubmit = (values: Object, dispatch: Dispatch) => dispatch(actions.verifySmsKey())

// istanbul ignore next
const enhance = compose(
  connect(({ app, auth }: State) => ({ app, auth }), actions),
  reduxForm({
    form: 'verifySms',
    onSubmit,
    validate,
  })
)

export default enhance(VerifySmsForm)
