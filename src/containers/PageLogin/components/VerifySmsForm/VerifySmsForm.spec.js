import React from 'react'
import { shallow } from 'enzyme'

import { AppModes } from '../../../../constants'
import { authActions as actions } from '../../../../redux'

import { VerifySmsForm, validate, onSubmit } from './VerifySmsForm'

describe('<VerifySmsForm />', () => {
  const props = {
    app: { mode: AppModes.TOKEN },
    sendSmsKey: jest.fn(),
    auth: {
      isVerifying: false,
      isSmsResendMessageVisible: false,
    },
  }

  it('renders correctly', () => {
    const tree = shallow(<VerifySmsForm {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders correctly when using the payment flow', () => {
    const app = { mode: AppModes.PAYMENT }
    const tree = shallow(<VerifySmsForm {...props} app={app} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders correctly during verification', () => {
    const auth = { ...props.auth, isVerifying: true }
    const tree = shallow(<VerifySmsForm {...props} auth={auth} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders correctly during resending', () => {
    const auth = { ...props.auth, isSmsResendMessageVisible: true }
    const tree = shallow(<VerifySmsForm {...props} auth={auth} />)

    expect(tree).toMatchSnapshot()
  })

  it('resends verification sms', () => {
    const tree = shallow(<VerifySmsForm {...props} />)

    tree.find('[testid="btnResend"]').simulate('click')

    expect(props.sendSmsKey).toBeCalledWith({ isResending: true })
  })

  it('dispatches verify sms action on submit', () => {
    const dispatch = jest.fn()

    onSubmit({}, dispatch)

    expect(dispatch).toBeCalledWith(actions.verifySmsKey())
  })
})

describe('VerifySmsForm validation', () => {
  it('requires smsKey', () => {
    const errors = validate({})

    expect(errors.smsKey).toBeDefined()
  })
})
