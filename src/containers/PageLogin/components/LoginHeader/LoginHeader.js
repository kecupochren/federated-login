// @flow

import React from 'react'
import get from 'lodash/get'

import { Text } from '../../../../components'

type Props = {
  mode: string,
  queryData: ?Object,
}

export const LoginHeader = ({ mode, queryData }: Props) => (
  <div>
    <Text
      center
      message={{ id: `page.login.${mode}.title` }}
      values={{ thirdParty: get(queryData, 'client_name') }}
    />
  </div>
)

export default LoginHeader
