import React from 'react'
import { shallow } from 'enzyme'
import { LoginHeader } from './LoginHeader'

describe('<LoginHeader />', () => {
  const props = {
    queryData: { client_name: 'Foo' },
  }

  it('renders correctly', () => {
    const tree = shallow(<LoginHeader {...props} />)

    expect(tree).toMatchSnapshot()
  })
})
