// @flow

import React from 'react'

import { Button } from '../../../../components'

type Props = {
  mode: string,
  handleConfirm: () => mixed,
  handleReject: () => mixed,
  hasReturnUrl: boolean,
}

export const ConfirmForm = ({ mode, handleConfirm, handleReject, hasReturnUrl }: Props) => (
  <div>
    <Button primary message={{ id: `button.confirm_access.${mode}` }} onClick={handleConfirm} />

    {hasReturnUrl && (
      <Button center anchor message={{ id: `button.reject_access` }} onClick={handleReject} />
    )}
  </div>
)

export default ConfirmForm
