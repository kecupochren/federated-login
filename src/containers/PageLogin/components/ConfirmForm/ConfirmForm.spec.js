import React from 'react'
import { shallow } from 'enzyme'
import { ConfirmForm } from './ConfirmForm'

describe('<ConfirmForm />', () => {
  const props = {
    handleConfirm: jest.fn(),
  }

  it('renders correctly', () => {
    const tree = shallow(<ConfirmForm {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders correctly when returnUrl is present', () => {
    const hasReturnUrl = true
    const tree = shallow(<ConfirmForm {...props} hasReturnUrl={hasReturnUrl} />)

    expect(tree).toMatchSnapshot()
  })
})
