import React from 'react'
import { shallow } from 'enzyme'

import { authActions } from '../../../../redux'

import { LoginForm, validate, onSubmit } from './LoginForm'

describe('<LoginForm />', () => {
  const props = {
    clearLoginError: jest.fn(),
    loginReset: jest.fn(),
    auth: { isLoggedIn: false },
  }

  it('renders correctly', () => {
    const tree = shallow(<LoginForm {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders correctly when isLoggedIn', () => {
    const auth = { isLoggedIn: true }
    const tree = shallow(<LoginForm {...props} auth={auth} />)

    expect(tree).toMatchSnapshot()
  })

  it("resets login error on inputs' change", () => {
    const tree = shallow(<LoginForm {...props} />)

    tree.find('[testid="inputId"]').simulate('change')
    tree.find('[testid="inputPassword"]').simulate('change')

    expect(props.clearLoginError).toHaveBeenCalledTimes(2)
  })

  it("resets isLoggedIn on inputs' change", () => {
    const auth = { isLoggedIn: true }
    const tree = shallow(<LoginForm {...props} auth={auth} />)

    tree.find('[testid="inputId"]').simulate('change')
    tree.find('[testid="inputPassword"]').simulate('change')

    expect(props.loginReset).toHaveBeenCalledTimes(2)
  })

  it('dispatches login action on submit', () => {
    const dispatch = jest.fn()

    onSubmit({}, dispatch)

    expect(dispatch).toBeCalledWith(authActions.login())
  })
})

describe('LoginForm validation', () => {
  it('requires ID and password', () => {
    const errors = validate({})

    expect(errors.id).toBeDefined()
    expect(errors.password).toBeDefined()
  })

  it('requires password of min length 4', () => {
    const errors = validate({ password: '123' })

    expect(errors.password).toBeDefined()
  })
})
