// @flow

import * as React from 'react'
import { compose } from 'recompose'
import { reduxForm } from 'redux-form'
import { connect } from 'react-redux'

import { Message, Input, Button } from '../../../../components'
import { authActions } from '../../../../redux'
import { required, minLength } from '../../../../utils/validation'
import type { Action, Dispatch, AuthState } from '../../../../types'

type Props = {
  handleSubmit: () => mixed,
  clearLoginError: () => Action,
  loginReset: () => Action,
  auth: AuthState,
  valid: boolean,
}

export class LoginForm extends React.Component<Props> {
  handleInputChange = () => {
    this.props.clearLoginError()

    if (this.props.auth.isLoggedIn) {
      this.props.loginReset()
    }
  }

  props: Props

  render() {
    const { handleSubmit, auth, valid }: Props = this.props

    return (
      <form onSubmit={handleSubmit}>
        <Input
          name="id"
          messages={{ label: { id: 'label.id' } }}
          testid="inputId"
          onChange={this.handleInputChange}
        />

        <Input
          name="password"
          type="password"
          testid="inputPassword"
          messages={{ label: { id: 'label.password' } }}
          onChange={this.handleInputChange}
        />

        <Message error message={auth.loginError} isVisible={auth.loginError} />

        <Button
          disabled={!valid}
          submitting={auth.isLoggingIn}
          type="submit"
          message={{ id: 'button.send_sms_key' }}
          color="primary"
        />
      </form>
    )
  }
}

export const validate = ({ id, password }: Object) => ({
  id: required(id),
  password: required(password) || minLength(4)(password),
})

export const onSubmit = (values: Object, dispatch: Dispatch) => dispatch(authActions.login())

// istanbul ignore next
const enhance = compose(
  connect(({ auth }) => ({ auth }), {
    clearLoginError: authActions.clearLoginError,
    loginReset: authActions.loginReset,
  }),
  reduxForm({
    form: 'login',
    onSubmit,
    validate,
  })
)

export default enhance(LoginForm)
