import React from 'react'
import { shallow } from 'enzyme'

import { AppModes } from '../../constants'

import { PageSuccess } from './PageSuccess'

describe('<PageSuccess />', () => {
  const props = {
    app: { mode: AppModes.TOKEN },
    returnToOrigin: jest.fn(),
  }

  describe('render', () => {
    describe('when TOKEN AppMode', () => {
      it('matches snapshot', () => {
        const tree = shallow(<PageSuccess {...props} />)
        expect(tree).toMatchSnapshot()
      })
    })

    describe('when PAYMENT AppMode', () => {
      const app = { mode: AppModes.PAYMENT }
      it('matches snapshot', () => {
        const tree = shallow(<PageSuccess {...props} app={app} />)
        expect(tree).toMatchSnapshot()
      })
    })

    describe('when IDENTITY_CONFIRMATION AppMode', () => {
      const app = { mode: AppModes.IDENTITY_CONFIRMATION }
      it('matches snapshot', () => {
        const tree = shallow(<PageSuccess {...props} app={app} />)
        expect(tree).toMatchSnapshot()
      })
    })
  })

  describe('componentDidMount', () => {
    it('calls returnToOrigin', () => {
      const tree = shallow(<PageSuccess {...props} />)

      jest.useFakeTimers()
      tree.instance().componentDidMount()
      jest.runAllTimers()

      expect(props.returnToOrigin).toBeCalled()
    })
  })
})
