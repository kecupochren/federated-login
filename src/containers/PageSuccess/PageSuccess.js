// @flow

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import get from 'lodash/get'

import { Page, Text, SuccessIcon } from '../../components'
import { appActions } from '../../redux/'
import type { Action, State, AppState } from '../../types/'
import { PermissionScopes as scopes } from '../../constants'

type Props = {
  app: AppState,
  returnToOrigin: () => Action,
}

export class PageSuccess extends PureComponent<Props, *> {
  componentDidMount = () => setTimeout(this.props.returnToOrigin, 3000)

  props: Props

  render() {
    const { app: { mode, queryData } }: Props = this.props
    const scope = get(queryData, 'scope')
    const suffix = scope === scopes.KYC && mode === 'IDENTITY_CONFIRMATION' ? '.KYC' : ''

    return (
      <Page>
        <SuccessIcon />
        <Text center message={{ id: `page.success.${mode}.title${suffix}` }} />
      </Page>
    )
  }
}

const withStore = connect(({ app }: State) => ({ app }), {
  returnToOrigin: appActions.returnToOrigin,
})

export default withStore(PageSuccess)
