import React from 'react'
import { shallow } from 'enzyme'

import { PagePayment } from './PagePayment'

describe('<PagePayment />', () => {
  const props = {
    app: { initialized: false },
    payment: {
      isQueryValid: true,
      isPaymentDataLoaded: true,
      accountBalanceData: {},
    },
    initialize: jest.fn(),
    location: { search: '?foo=bar' },
  }

  it('renders correctly', () => {
    const tree = shallow(<PagePayment {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders initialized & loaded correctly', () => {
    const app = { initialized: true }
    const payment = { ...props.payment, isPaymentDataLoaded: true }
    const tree = shallow(<PagePayment {...props} app={app} payment={payment} />)

    expect(tree).toMatchSnapshot()
  })

  it('initializes with parsed query on mount', () => {
    const location = { search: '?foo=bar' }
    const tree = shallow(<PagePayment {...props} location={location} />)

    jest.useFakeTimers()
    tree.instance().componentDidMount()
    jest.runAllTimers()

    expect(props.initialize).toBeCalledWith({ foo: 'bar' })
  })
})
