import React from 'react'
import { shallow } from 'enzyme'
import { animateScroll as scroll } from 'react-scroll'

import { VerifyPaymentSmsForm } from './VerifyPaymentSmsForm'

describe('<VerifyPaymentSmsForm />', () => {
  const props = {
    payment: {
      isSmsSent: false,
      isSmsSending: false,
      isSmsVerifying: false,
      isPaymentSmsResendMessageVisible: true,
    },
  }

  it('renders correctly when sms is not sent', () => {
    const tree = shallow(<VerifyPaymentSmsForm {...props} />)

    expect(tree).toMatchSnapshot()
  })

  it('renders correctly when sms is sent', () => {
    const payment = { isSmsSent: true }
    const tree = shallow(<VerifyPaymentSmsForm {...props} payment={payment} />)

    expect(tree).toMatchSnapshot()
  })

  it('scrolls to the bottom of the page on sms sent', () => {
    const tree = shallow(<VerifyPaymentSmsForm {...props} />)
    const nextProps = { payment: { isSmsSent: true } }
    scroll.scrollTo = jest.fn()

    tree.instance().componentWillReceiveProps(nextProps)

    expect(scroll.scrollTo).toBeCalledWith(10000)
  })
})
