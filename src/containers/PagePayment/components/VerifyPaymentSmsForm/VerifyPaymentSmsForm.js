// @flow

import React, { PureComponent } from 'react'
import get from 'lodash/get'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { animateScroll as scroll } from 'react-scroll'

import { Button, Input, Message, Text } from '../../../../components'
import { required } from '../../../../utils/index'
import { paymentActions as actions } from '../../../../redux'
import type { Action, Dispatch, State as StateType, PaymentState } from '../../../../types'

type Props = {
  authorizePaymentFailure: () => Action,
  clearPaymentVerifyError: () => Action,
  handleSubmit: () => mixed,
  payment: PaymentState,
  sendPaymentSmsKey: (options: Object) => Action,
  valid: boolean,
}

type State = {
  hasScrolled: boolean,
}

export class VerifyPaymentSmsForm extends PureComponent<Props, State> {
  state: State = {
    hasScrolled: false,
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.payment.isSmsSent && !this.state.hasScrolled) {
      this.setState({ hasScrolled: true })

      const target = get(document, 'body.scrollHeight') || 10000

      scroll.scrollTo(target)
    }
  }

  props: Props

  render() {
    const {
      authorizePaymentFailure,
      clearPaymentVerifyError,
      handleSubmit,
      payment,
      sendPaymentSmsKey,
      valid,
    } = this.props

    return (
      <form onSubmit={handleSubmit}>
        {!payment.isSmsSent ? (
          <Button
            submitting={payment.isSmsSending}
            onClick={sendPaymentSmsKey}
            message={{ id: 'button.send_sms_key' }}
            type="button"
          />
        ) : (
          <div>
            <div className="mb-2" />

            <Text message={{ id: 'page.payment.sms_notice' }} />

            <Input
              messages={{ label: { id: 'label.sms_key' } }}
              name="smsKey"
              onChange={clearPaymentVerifyError}
            />

            <Message error message={payment.verifySmsError} isVisible={payment.verifySmsError} />

            <Button
              disabled={!valid}
              message={{ id: 'button.verify_sms_key' }}
              primary
              submitting={payment.isSmsVerifying}
              type="submit"
            />

            <Message
              success
              message={{ id: 'success.resend_sms' }}
              isVisible={payment.isPaymentSmsResendMessageVisible}
            />

            <Button
              anchor
              disabled={payment.isSmsSending}
              id="btnResend"
              message={{ id: 'button.new_sms_key' }}
              onClick={() => sendPaymentSmsKey({ isResending: true })}
              type="button"
            />
          </div>
        )}

        <Button
          onClick={authorizePaymentFailure}
          anchor
          message={{ id: 'button.return_to_vendor' }}
        />
      </form>
    )
  }
}

// istanbul ignore next
const enhance = compose(
  connect(({ payment }: StateType) => ({ payment }), actions),
  reduxForm({
    form: 'verifyPaymentSms',
    onSubmit: (values: Object, dispatch: Dispatch) => dispatch(actions.verifyPaymentSmsKey()),
    validate: ({ smsKey }: Object) => ({ smsKey: required(smsKey) }),
  })
)

export default enhance(VerifyPaymentSmsForm)
