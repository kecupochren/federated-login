// @flow

import React from 'react'
import moment from 'moment'
import get from 'lodash/get'

import { Text } from '../../../../components'

type Props = {
  paymentData: ?Object,
}

export const PaymentDataInternational = ({ paymentData }: Props) => (
  <div>
    <Text bold message={{ id: 'page.payment.title' }} />

    <div>
      <Text small noMargin message={{ id: 'page.payment.own_account' }} />
      <Text bold className="mb-1">
        {get(paymentData, 'debtorAccount.identification.other.identification', '')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.amount' }} />
      <Text bold className="mb-1">
        {`${get(paymentData, 'amount.instructedAmount.value', '')} 
          ${get(paymentData, 'amount.instructedAmount.currency', '')}`}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.due_date' }} />
      <Text bold className="mb-1">
        {moment(get(paymentData, 'requestedExecutionDate'), '').format('DD.MM.YYYY')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.expenses' }} />
      <Text bold className="mb-1">
        {get(paymentData, 'chargeBearer', '')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.iban' }} />
      <Text bold className="mb-1">
        {get(paymentData, 'creditorAccount.identification.iban', '') ||
          get(paymentData, 'creditorAccount.identification.other.identification', '')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.receiver' }} />
      <Text bold className="mb-1" breakLine>
        {get(paymentData, 'creditor', '')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.receiver_message' }} />
      <Text bold breakWord className="mb-1">
        {get(paymentData, 'remittanceInformation.unstructured', '')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.payer' }} />
      <Text bold className="mb-1">
        {get(paymentData, 'paymentIdentification.endToEndIdentification', '')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.bic' }} />
      <Text bold className="mb-1">
        {get(paymentData, 'creditorAgent.bic', '')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.bank' }} />
      <Text bold className="mb-1">
        {get(paymentData, 'creditorAgent.bankInfo', '')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.country' }} />
      <Text bold className="mb-1">
        {get(paymentData, 'creditorAgent.country', '')}
      </Text>
    </div>

    <div>
      <Text small noMargin message={{ id: 'page.payment.info' }} />
      <Text bold className="mb-1">
        {get(paymentData, 'transactionConfirmation', '')}
      </Text>
    </div>

    <hr />
  </div>
)

export default PaymentDataInternational
