import React from 'react'
import { shallow } from 'enzyme'
import { PaymentDataInternational } from './PaymentDataInternational'

import mockGetPaymentDataRes from '../../../../../mockserver/mocks/secured/psd/pisp/my/payments/456/GET'

describe('<PaymentDataInternational />', () => {
  const props = {
    paymentData: mockGetPaymentDataRes.body.responseObject,
  }

  it('renders correctly', () => {
    const tree = shallow(<PaymentDataInternational {...props} />)

    expect(tree).toMatchSnapshot()
  })
})
