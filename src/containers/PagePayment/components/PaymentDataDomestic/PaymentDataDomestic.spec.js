import React from 'react'
import { shallow } from 'enzyme'
import { PaymentDataDomestic } from './PaymentDataDomestic'

import mockGetPaymentDataRes from '../../../../../mockserver/mocks/secured/psd/pisp/my/payments/123/GET'
import accountBalanceRes from '../../../../../mockserver/mocks/secured/psd/aisp/my/accounts/021452099/balance/GET'

describe('<PaymentDataDomestic />', () => {
  const props = {
    paymentData: mockGetPaymentDataRes.body.responseObject,
    accountBalanceData: accountBalanceRes.body.responseObject,
  }

  it('renders correctly', () => {
    const tree = shallow(<PaymentDataDomestic {...props} />)

    expect(tree).toMatchSnapshot()
  })
})
