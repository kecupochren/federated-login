// @flow

import React from 'react'
import moment from 'moment'
import numeral from 'numeral'
import get from 'lodash/get'

import { Text } from '../../../../components'

type Props = {
  paymentData: ?Object,
  accountBalanceData: ?Object,
}

export const PaymentDataDomestic = ({ accountBalanceData, paymentData }: Props) => {
  // prettier-ignore
  const symbolsRaw = get(paymentData, 'remittanceInformation.structured.creditorReferenceInformation.reference')
  const symbolsSplit = symbolsRaw && symbolsRaw.split(',')
  const symbols = {
    variable: symbolsSplit && symbolsSplit[0].substring(3),
    constant: symbolsSplit && symbolsSplit[1].substring(3),
    specific: symbolsSplit && symbolsSplit[2].substring(3),
  }

  // lol
  Object.keys(symbols).forEach(key => {
    if (symbols[key] === 'null') {
      symbols[key] = null
    }
  })

  const balance = get(accountBalanceData, 'balances[0].amount.value')
  const currency = get(accountBalanceData, 'balances[0].amount.currency')

  return (
    <div>
      <Text bold message={{ id: 'page.payment.title' }} />

      {get(paymentData, 'amount.instructedAmount.value') && (
        <div>
          <Text small noMargin message={{ id: 'page.payment.amount' }} />
          <Text bold className="mb-1">
            {get(paymentData, 'amount.instructedAmount.value')}{' '}
            {get(paymentData, 'amount.instructedAmount.currency')}
          </Text>
        </div>
      )}

      {symbols.variable && (
        <div>
          <Text small noMargin message={{ id: 'page.payment.variable_symbol' }} />
          <Text bold className="mb-1">
            {symbols.variable}
          </Text>
        </div>
      )}

      {symbols.constant && (
        <div>
          <Text small noMargin message={{ id: 'page.payment.constant_symbol' }} />
          <Text bold className="mb-1">
            {symbols.constant}
          </Text>
        </div>
      )}

      {symbols.specific && (
        <div>
          <Text small noMargin message={{ id: 'page.payment.specific_symbol' }} />
          <Text bold className="mb-1">
            {symbols.specific}
          </Text>
        </div>
      )}

      {get(paymentData, 'requestedExecutionDate') && (
        <div>
          <Text small noMargin message={{ id: 'page.payment.due_date' }} />
          <Text bold className="mb-1">
            {moment(get(paymentData, 'requestedExecutionDate')).format('DD.MM.YYYY')}
          </Text>
        </div>
      )}

      {get(paymentData, 'remittanceInformation.unstructured') && (
        <div>
          <Text small noMargin message={{ id: 'page.payment.receiver_message' }} />
          <Text bold className="mb-1" breakWord>
            {get(paymentData, 'remittanceInformation.unstructured')}
          </Text>
        </div>
      )}

      <hr />

      {get(paymentData, 'debtorAccount.identification.other.identification') && (
        <div>
          <Text small noMargin message={{ id: 'page.payment.from_account' }} />
          <Text bold className="mb-1">
            {get(paymentData, 'debtorAccount.identification.other.identification')}
          </Text>
        </div>
      )}

      <Text small noMargin message={{ id: 'page.payment.account_balance' }} />
      <Text bold className="mb-1">
        {balance && numeral(balance).format('0 0.00')} {currency}
      </Text>

      {/* <Text small noMargin message={{ id: 'page.payment.receiver_name' }} />
      <Text bold className="mb-1">
        Alza s.k.
      </Text> */}

      {(get(paymentData, 'creditorAccount.identification.other.identification') ||
        get(paymentData, 'creditorAccount.identification.iban')) && (
        <div>
          <Text small noMargin message={{ id: 'page.payment.receiver_bank_number' }} />
          <Text bold className="mb-1">
            {get(paymentData, 'creditorAccount.identification.other.identification') ||
              get(paymentData, 'creditorAccount.identification.iban')}
          </Text>
        </div>
      )}

      {/* <Text small noMargin message={{ id: 'page.payment.receiver_bank_code' }} />
      <Text bold className="mb-1">
        KB
      </Text> */}

      <hr />
    </div>
  )
}

export default PaymentDataDomestic
