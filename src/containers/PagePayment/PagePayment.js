// @flow

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import qs from 'query-string'

import { Page, Spinner } from '../../components'
import { appActions } from '../../redux'
import { isInternationalPayment } from '../../utils'
import type { Action, State, AppState, PaymentState } from '../../types'

import { PaymentDataDomestic, PaymentDataInternational, VerifyPaymentSmsForm } from './components'

type Props = {
  app: AppState,
  payment: PaymentState,
  initialize: (queryData: Object) => Action,
  location: { search: string },
}

export class PagePayment extends PureComponent<Props, *> {
  static defaultProps = {
    location: { search: '' },
  }

  componentDidMount() {
    const { app, location, initialize }: Props = this.props

    if (!app.initialized) {
      const queryData = qs.parse(location.search)

      setTimeout(() => initialize(queryData), 10)
    }
  }

  props: Props

  render() {
    const { app, payment: { isPaymentDataLoaded, paymentData, accountBalanceData } } = this.props

    const isReady = app.initialized && isPaymentDataLoaded

    return (
      <Page compact className="text--center">
        <Spinner isVisible={!isReady} />

        {isReady && (
          <div>
            {isInternationalPayment(paymentData) ? (
              <PaymentDataInternational paymentData={paymentData} />
            ) : (
              <PaymentDataDomestic
                paymentData={paymentData}
                accountBalanceData={accountBalanceData}
              />
            )}
            <VerifyPaymentSmsForm />
          </div>
        )}
      </Page>
    )
  }
}

// istanbul ignore next
const withStore = connect(
  ({ app, payment }: State) => ({
    app,
    payment,
  }),
  {
    initialize: appActions.initialize,
  }
)

export default withStore(PagePayment)
