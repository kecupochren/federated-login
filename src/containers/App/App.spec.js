import React from 'react'
import { shallow } from 'enzyme'

import { App } from './App'

describe('App', () => {
  const props = {
    setEnvironment: jest.fn(),
  }

  it('renders without errors', () => {
    const wrapper = shallow(<App {...props} />)

    expect(wrapper).toBeDefined()
  })

  it('sets the environemnt on mount', () => {
    const wrapper = shallow(<App {...props} />)

    wrapper.instance().componentDidMount()

    expect(props.setEnvironment).toBeCalled()
  })
})
