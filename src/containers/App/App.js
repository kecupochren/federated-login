// @flow

import React, { Component } from 'react'
import store from 'store'
import get from 'lodash/get'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import { IntlProvider } from 'react-intl'
import { ConnectedRouter } from 'react-router-redux'

import '../../assets/css/global.scss'
import { history } from '../../redux/store'
import { configActions } from '../../redux'
import * as Pages from '../../containers'
import { Routes } from '../../constants'
import * as messages from '../../i18n'
import type { State } from '../../types'

type Props = {
  setEnvironment: (env: string) => mixed,
  lang: string,
}

export class App extends Component<Props, *> {
  componentDidMount() {
    const env = get(window, 'app_config.REACT_APP_ENV') || store.get('env') || 'DEV'

    this.props.setEnvironment(env)
  }

  props: Props

  render() {
    const { lang } = this.props

    return (
      <IntlProvider key={lang} locale={lang} messages={messages[lang]}>
        <div className="app">
          <ConnectedRouter history={history}>
            <div>
              <Route exact path="/" component={Pages.PageLogin} />
              <Route exact path={Routes.LOGIN} component={Pages.PageLogin} />
              <Route exact path={Routes.PAYMENT} component={Pages.PagePayment} />
              <Route exact path={Routes.SUCCESS} component={Pages.PageSuccess} />
              <Route exact path={Routes.ERROR} component={Pages.PageError} />
            </div>
          </ConnectedRouter>
        </div>
      </IntlProvider>
    )
  }
}

const mapStateToProps = ({ config }: State) => ({
  lang: config.lang,
})

const withStore = connect(mapStateToProps, {
  setEnvironment: configActions.setEnvironment,
})

export default withStore(App)
