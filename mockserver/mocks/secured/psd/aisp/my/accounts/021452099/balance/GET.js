const status = 200
const headers = require('../../../../../../../../cors-json.js')

const body = {
  status: 'OK',
  responseObject: {
    balances: [
      {
        amount: {
          value: '9999.99',
          currency: 'CZK',
        },
      },
    ],
  },
}

module.exports = {
  status,
  body,
  headers,
}
