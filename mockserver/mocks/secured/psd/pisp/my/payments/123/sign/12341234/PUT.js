const status = 200
const headers = require('../../../../../../../../../cors-json.js')

const body = {
  status: 'OK',
  responseObject: { code: 'TRANSACTION_MISSING' },
}

module.exports = {
  status,
  body,
  headers,
}
