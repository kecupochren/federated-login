const status = 200
const headers = require('../../../../../../../cors-json.js')

// domestic payment data
const body = {
  status: 'OK',
  responseObject: {
    paymentIdentification: {
      transactionIdentification: '301.15.337862805.1',
    },
    paymentTypeInformation: {
      instructionPriority: 'NORM',
    },
    amount: {
      instructedAmount: {
        value: 10,
        currency: 'CZK',
      },
      equivalentAmount: {
        value: 10,
        currency: 'CZK',
      },
    },
    requestedExecutionDate: '2017-11-29',
    debtorAccount: {
      identification: {
        other: {
          identification: '021452099/0600',
        },
      },
      currency: 'CZK',
    },
    creditorAccount: {
      identification: {
        other: {
          identification: '000000 1000377902/3500',
        },
      },
    },
    remittanceInformation: {
      structured: {
        creditorReferenceInformation: {
          reference: 'VS:123,KS:456,SS:789',
        },
      },
      unstructured: 'test',
    },
    instructionStatus: 'FOR_SIGNING',
  },
}

module.exports = {
  status,
  body,
  headers,
}
