const status = 200
const headers = require('../../../../../../../../../cors-json.js')

const body = {
  status: 'OK',
  responseObject: null,
}

module.exports = {
  status,
  body,
  headers,
}
