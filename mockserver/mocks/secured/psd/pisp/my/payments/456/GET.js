const status = 200
const headers = require('../../../../../../../cors-json.js')

// international payment data
const body = {
  status: 'OK',
  responseObject: {
    paymentIdentification: {
      endToEndIdentification: 'REFERENCE',
      transactionIdentification: 'Dv9NxO2NG0GAJQxch3fojMjoKrbTLm58MyKr3byA',
    },
    paymentTypeInformation: { instructionPriority: 'HIGH', serviceLevel: { code: 'FPOE' } },
    amount: { instructedAmount: { value: 50, currency: 'EUR' } },
    requestedExecutionDate: '2018-01-24',
    chargeBearer: 'SHA',
    debtorAccount: {
      identification: { other: { identification: '0214525099/0600' } },
      currency: 'CZK',
    },
    creditorAccount: { identification: { iban: 'IT15I0572821570470571194423' } },
    creditor: 'JMENO\nULICE\nMESTO',
    creditorAgent: {
      bic: 'BPVIIT21470',
      bankInfo: 'BANCA POPOLARE DI VICENZA SPA\nPIAZZA G. BORSI, 8\n59100 PRATO',
      country: 'Italy',
    },
    remittanceInformation: {
      unstructured: 'ZPRAVA',
      structured: { creditorReferenceInformation: { reference: 'VS:null,KS:null,SS:null' } },
    },
    instructionStatus: 'FOR_SIGNING',
    transactionConfirmation: 'gabriela.polanska@moneta.cz',
  },
}

module.exports = {
  status,
  body,
  headers,
}
