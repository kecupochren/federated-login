const status = 200
const headers = require('../../../../cors-json.js')

const body = {
  status: 'OK',
  responseObject: {},
}

module.exports = {
  status,
  body,
  headers,
}
