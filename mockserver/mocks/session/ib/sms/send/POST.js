const status = 200
const headers = require('../../../../../cors-json.js')

const body = {
  status: 'OK',
  responseObject: {
    smsSendCertValue: 'SMS_SENT',
  },
}

module.exports = {
  status,
  body,
  headers,
}
