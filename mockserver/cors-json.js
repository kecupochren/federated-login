module.exports = [
  'Access-Control-Allow-Origin: http://localhost:3000',
  'Access-Control-Allow-Credentials: true',
  'Access-Control-Allow-Headers: *, Channel, Cache-Control, Content-Type',
  'Content-Type: application/json; charset=utf-8',
]
