const createJestConfig = require('@prague-digi/react-scripts/scripts/utils/createJestConfig')
const path = require('path')
// jest is provided by CRA-Prague
const jest = require('jest') // eslint-disable-line
const packageJson = require('../package.json')

const arg = process.argv[2]
const silent = arg && (arg === '--silent' || arg === '--s') ? '--silent' : ''

process.env.NODE_ENV = 'test'

// we generate the default config from react-scripts
const baseJestConfig = createJestConfig(
  relativePath =>
    path.resolve(__dirname, '../node_modules/@prague-digi/react-scripts/', relativePath),
  path.resolve('.'),
  false
)

// then we merge it with the jest config defined
// in the package.json, which defines the coverage limitation
const completeConfig = Object.assign({}, baseJestConfig, packageJson.jest)

jest.run([
  '--config',
  JSON.stringify(completeConfig),
  '--coverage',
  '--env=jsdom',
  '--no-watchman',
  silent,
])
