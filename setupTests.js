const { configure } = require('enzyme') // eslint-disable-line import/no-extraneous-dependencies
const Adapter = require('enzyme-adapter-react-16') // eslint-disable-line import/no-extraneous-dependencies

configure({ adapter: new Adapter() })

Object.defineProperty(document, 'getElementById', {
  value: () => document.createElement('div'),
})
